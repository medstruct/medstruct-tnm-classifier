//package com.github.putssander.medstruct.tnmclassifier;
//
//import com.fasterxml.jackson.databind.JsonNode;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.github.putssander.medstruct.tnmclassifier.service.TnmClassificationService;
//import com.github.putssander.medstruct.tnmclassifier.classification.TnmClassification;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.io.IOException;
//import java.nio.file.Files;
//import java.nio.file.Paths;
//
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class TnmClassifierApplicationTests {
//
//	private static final Logger logger = LoggerFactory.getLogger(TnmClassifierApplicationTests.class);
//
//	private static final String jsonNlpDocPath = "src/test/resources/json-nlp.json";
//	private static final String jsonNlpDocResultPath = "src/test/resources/json-nlp-result.json";
//
//	private static final String jsonNlpT2DocPath = "src/test/resources/json-nlp-t2-inv-main-broch-measurement-result.json";
//	private static final String jsonNlpT2DocResultPath = "src/test/resources/json-nlp-t2-inv-main-broch-tnm-result.json";
//
//	private static final String case11 = "src/test/resources/ignored/53.json";
//	private static final String case11Result = "src/test/resources/ignored/53-result.json";
//
//	@Autowired
//	private TnmClassificationService tnmClassificationService;
//
//	@Autowired
//	private ObjectMapper objectMapper;
//
//	@Test
//	public void contextLoads() {
//	}
//
//
//	@Test
//	public void classifyTnm() throws IOException {
//		String jsonNlp = new String(Files.readAllBytes(Paths.get(jsonNlpDocPath)));
//		JsonNode jsonNlpNode = objectMapper.readTree(jsonNlp);
//		JsonNode document = jsonNlpNode.get("documents").get(0);
//		TnmClassification tnm = tnmClassificationService.classify(document);
//		logger.info("tnm={}",tnm);
//
////		boolean foundMeasurementLoincValue = false;
////		double codeValue = 70;
////		for(MeasurementExpression measurement : measurements){
////			if(measurement.getCodeValue() != null && measurement.getCodeValue() == codeValue)
////				foundMeasurementLoincValue = true;
////		}
////
////		assertTrue(foundMeasurementLoincValue);
////
////		document = tnmClassificationService.addTnm(document, measurements);
//////		((ObjectNode) jsonNlpNode).putArray("documents").add(document);
////		((ArrayNode) jsonNlpNode.withArray("documents")).set(0, document);
////		String jsonNlpResult = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNlpNode);
////		try (PrintStream out = new PrintStream(new FileOutputStream(jsonNlpDocResultPath))) {
////			out.print(jsonNlpResult);
////		}
//	}
//
//	@Test
//	public void classifyTnmT2() throws IOException {
//		String jsonNlp = new String(Files.readAllBytes(Paths.get(jsonNlpT2DocPath)));
//		JsonNode jsonNlpNode = objectMapper.readTree(jsonNlp);
//		JsonNode document = jsonNlpNode.get("documents").get(0);
//		TnmClassification tnm = tnmClassificationService.classify(document);
//		logger.info("tnm={}",tnm);
//
////		boolean foundMeasurementLoincValue = false;
////		double codeValue = 70;
////		for(MeasurementExpression measurement : measurements){
////			if(measurement.getCodeValue() != null && measurement.getCodeValue() == codeValue)
////				foundMeasurementLoincValue = true;
////		}
////
////		assertTrue(foundMeasurementLoincValue);
////
////		document = tnmClassificationService.addTnm(document, measurements);
//////		((ObjectNode) jsonNlpNode).putArray("documents").add(document);
////		((ArrayNode) jsonNlpNode.withArray("documents")).set(0, document);
////		String jsonNlpResult = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNlpNode);
////		try (PrintStream out = new PrintStream(new FileOutputStream(jsonNlpDocResultPath))) {
////			out.print(jsonNlpResult);
////		}
//	}
//
//
//	@Test
//	public void classifyCase11() throws IOException {
//		String jsonNlp = new String(Files.readAllBytes(Paths.get(case11)));
//		JsonNode jsonNlpNode = objectMapper.readTree(jsonNlp);
//		JsonNode document = jsonNlpNode.get("documents").get(0);
//		TnmClassification tnm = tnmClassificationService.classify(document);
//		logger.info("tnm={}", tnm);
//	}
//
//}
