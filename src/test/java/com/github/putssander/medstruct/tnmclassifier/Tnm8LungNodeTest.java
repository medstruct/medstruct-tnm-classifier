//package com.github.putssander.medstruct.tnmclassifier;
//
//import org.junit.runner.RunWith;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import static org.junit.Assert.assertEquals;
//
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//@ActiveProfiles("rest")
//public class Tnm8LungNodeTest {
//
//	private static final Logger logger = LoggerFactory.getLogger(Tnm8LungNodeTest.class);
//
//
////
////	@Test
////	public void contextLoads() {
////	}
////
////
////	@Test
////	/**
////	 * 	Ipsilateral peribronchial and/or hilar lymph nodes 10R-14R
////	 */
////	public void classifyTnmN1RightLung() {
////		List<LymphNode> lymphNodeList = new ArrayList<>();
////		lymphNodeList.add(new LymphNode(ThoracicLymphRegion.HILAR, 1.1,"cm", BodySide.RIGHT, null, null));
////		lymphNodeList.add(new LymphNode(ThoracicLymphRegion.PERIBRONCHAL, 1.1,"cm", BodySide.RIGHT, null, null));
////		lymphNodeList.forEach(lymphNode -> {
////			logger.info("currentNode=" + lymphNode);
////			assertEquals("N1", LymphNodeService.classifyNTumorRight(lymphNode.getThoracicLymphRegion(), lymphNode.getBodySide()));
////		});
////	}
////
////	@Test
////	/**
////	 * Ipsilateral mediastinal and/or subcarinal lymph nodes 2R, 3aR, 3p, 4R, 7, 8R, 9R
////	 */
////	public void classifyTnmN2RightLung() {
////		List<LymphNode> lymphNodeList = new ArrayList<>();
////		lymphNodeList.add(new LymphNode(ThoracicLymphRegion.MEDIASTINAL, 1.1,"cm", BodySide.RIGHT, null, null));
////		lymphNodeList.add(new LymphNode(ThoracicLymphRegion.PRE_VERTEBRAL, 1.1,"cm", BodySide.NONE, null, null));
////		lymphNodeList.add(new LymphNode(ThoracicLymphRegion.SUBCARINAL, 1.1,"cm", BodySide.NONE, null, null));
////
////		lymphNodeList.forEach(lymphNode -> {
////			logger.info("currentNode=" + lymphNode);
////			assertEquals("N2", LymphNodeService.classifyNTumorRight(lymphNode.getThoracicLymphRegion(), lymphNode.getBodySide()));
////		});
////	}
////
////	@Test
////	/**
////	 * Contralateral mediastinal and/or hilar, as well as any supraclavicular lymph nodes 1, 2L, 3aL, 4L, 5, 6, 8L, 9L, 10L-14L
////	 */
////	public void classifyTnmN3RightLung() {
////		List<LymphNode> lymphNodeList = new ArrayList<>();
////		lymphNodeList.add(new LymphNode(ThoracicLymphRegion.SUPRACLAVICULAR, 1.1,"cm", BodySide.NONE, null, null));
////		lymphNodeList.add(new LymphNode(ThoracicLymphRegion.MEDIASTINAL, 1.1,"cm", BodySide.LEFT, null, null));
////		lymphNodeList.add(new LymphNode(ThoracicLymphRegion.AORTIC, 1.1,"cm", BodySide.NONE, null, null));
////		lymphNodeList.add(new LymphNode(ThoracicLymphRegion.HILAR, 1.1,"cm", BodySide.LEFT, null, null));
////		lymphNodeList.add(new LymphNode(ThoracicLymphRegion.PERIBRONCHAL, 1.1,"cm", BodySide.LEFT, null, null));
////		lymphNodeList.forEach(lymphNode -> {
////			logger.info("currentNode=" + lymphNode);
////			assertEquals( "N3", LymphNodeService.classifyNTumorRight(lymphNode.getThoracicLymphRegion(), lymphNode.getBodySide()));
////		});
////	}
////
////	@Test
////	public void classifyTnmN1Left() {
////		List<LymphNode> lymphNodeList = new ArrayList<>();
////		lymphNodeList.add(new LymphNode(ThoracicLymphRegion.HILAR, 1.1,"cm", BodySide.LEFT, null, null));
////		lymphNodeList.add(new LymphNode(ThoracicLymphRegion.PERIBRONCHAL, 1.1,"cm", BodySide.LEFT, null, null));
////		lymphNodeList.forEach(lymphNode -> {
////			logger.info("currentNode=" + lymphNode);
////			assertEquals("N1", LymphNodeService.classifyNTumorLeft(lymphNode.getThoracicLymphRegion(), lymphNode.getBodySide()));
////		});
////	}
////
////	@Test
////	/**
////	 * Ipsilateral mediastinal and/or subcarinal lymph nodes 2L, 3aL, 4L, 5, 6, 7, 8L, 9L
////	 */
////	public void classifyTnmN2Left() {
////		List<LymphNode> lymphNodeList = new ArrayList<>();
////		lymphNodeList.add(new LymphNode(ThoracicLymphRegion.MEDIASTINAL, 1.1,"cm", BodySide.LEFT, null, null));
////		lymphNodeList.add(new LymphNode(ThoracicLymphRegion.SUBCARINAL, 1.1,"cm", BodySide.NONE, null, null));
////		lymphNodeList.add(new LymphNode(ThoracicLymphRegion.AORTIC, 1.1,"cm", BodySide.NONE, null, null));
////		lymphNodeList.forEach(lymphNode -> {
////			logger.info("currentNode=" + lymphNode);
////			assertEquals("N2", LymphNodeService.classifyNTumorLeft(lymphNode.getThoracicLymphRegion(), lymphNode.getBodySide()));
////		});
////	}
////
////	@Test
////	/**
////	 * Contralateral mediastinal and/or hilar, as well as any supraclavicular lymph nodes 1, 2R, 3aR, 3pR, 4R, 8R, 9R, 10-14R
////	 */
////	public void classifyTnmN3Left() {
////		List<LymphNode> lymphNodeList = new ArrayList<>();
////		lymphNodeList.add(new LymphNode(ThoracicLymphRegion.SUPRACLAVICULAR, 1.1,"cm", BodySide.NONE, null, null));
////		lymphNodeList.add(new LymphNode(ThoracicLymphRegion.MEDIASTINAL, 1.1,"cm", BodySide.RIGHT, null, null));
////		lymphNodeList.add(new LymphNode(ThoracicLymphRegion.PRE_VERTEBRAL, 1.1,"cm", BodySide.NONE, null, null));
////		lymphNodeList.add(new LymphNode(ThoracicLymphRegion.HILAR, 1.1,"cm", BodySide.RIGHT, null, null));
////		lymphNodeList.add(new LymphNode(ThoracicLymphRegion.PERIBRONCHAL, 1.1,"cm", BodySide.RIGHT, null, null));
////		lymphNodeList.forEach(lymphNode -> {
////			logger.info("currentNode=" + lymphNode);
////			assertEquals("N3", LymphNodeService.classifyNTumorLeft(lymphNode.getThoracicLymphRegion(), lymphNode.getBodySide()));
////		});
////	}
//
//}
