package com.github.putssander.medstruct.tnmclassifier.service;

import com.github.putssander.medstruct.tnmclassifier.classification.LymphNode;
import com.github.putssander.medstruct.tnmclassifier.constants.*;
import com.github.putssander.medstruct.tnmclassifier.configuration.properties.ThoracicLymphNodeStationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.github.putssander.medstruct.tnmclassifier.constants.BodySide.*;
import static com.github.putssander.medstruct.tnmclassifier.constants.ThoracicLymphNodeClassificationMethod.*;
import static com.github.putssander.medstruct.tnmclassifier.constants.ThoracicLymphNodeStation.*;

@Service
@EnableConfigurationProperties({ThoracicLymphNodeStationProperties.class})
public class LymphNodeClassificationService {

    private ThoracicLymphNodeStationProperties thoracicLymphNodeStationProperties;


    public LymphNodeClassificationService(ThoracicLymphNodeStationProperties thoracicLymphNodeStationProperties) {
        this.thoracicLymphNodeStationProperties = thoracicLymphNodeStationProperties;
    }

    private static final Logger logger = LoggerFactory.getLogger(LymphNodeClassificationService.class);

    public static String classifyN(BodySide primaryTumorBodySide, List<LymphNode> lymphNodes) {
        String nStage = "Nx";
        if(lymphNodes.isEmpty())
            return "N0";

        for(LymphNode lymphNode : lymphNodes) {
            lymphNode.setThoracicLymphNodePathologicScheme(getThoracicLymphNodeScheme(lymphNode));

            if(lymphNode.isAvide() != null && !lymphNode.isAvide()) {
                nStage = getClassification(nStage, "N0");
                continue;
            }

            switch (primaryTumorBodySide) {
                case LEFT:
                    nStage = getClassification(nStage, classifyNTumorLeft(lymphNode.getThoracicLymphNodePathologicScheme(), lymphNode.getBodySide()));
                    break;
                case RIGHT:
                    nStage = getClassification(nStage, classifyNTumorRight(lymphNode.getThoracicLymphNodePathologicScheme(), lymphNode.getBodySide()));
                    break;
                case NONE:
                case UNKNOWN:
                    nStage = getClassification(nStage, classifyNTumorUnknown(lymphNode.getThoracicLymphNodePathologicScheme()));
                    break;
            }
        }
        return nStage;
    }

    private static String getClassification(String currentN, String newN){
        List<String> nOrder = Arrays.asList("N4", "N3", "N2", "N1", "N0");
        for(String n : nOrder){
            if(currentN.equals(n) || newN.equals(n))
                    return n;
        }
        return "Nx";
    }


    public static String classifyNTumorRight(ThoracicLymphNodePathologicScheme thoracicLymphNodePathologicScheme, BodySide lymphNodeBodySide) {
        switch (thoracicLymphNodePathologicScheme){
            case TWO:
                return "N2";
            case THREE:
                return "N3";
            case DEFAULT:
                if(lymphNodeBodySide.equals(RIGHT))
                    return "N2";
                else
                    return "N3";
            case AORTIC:
                return "N3";
            case N1:
                if(lymphNodeBodySide.equals(RIGHT))
                    return "N1";
                else
                    return "N3";
            default:
                return "Nx";
        }
    }

    public static String classifyNTumorLeft(ThoracicLymphNodePathologicScheme thoracicLymphNodePathologicScheme, BodySide lymphNodeBodySide) {
        switch (thoracicLymphNodePathologicScheme){
            case TWO:
                return "N2";
            case THREE:
                return "N3";
            case DEFAULT:
                if(lymphNodeBodySide.equals(LEFT))
                    return "N2";
                else
                    return "N3";
            case AORTIC:
                return "N2";
            case N1:
                if(lymphNodeBodySide.equals(LEFT))
                    return "N1";
                else
                    return "N3";
            default:
                return "Nx";
        }
    }

    public static String classifyNTumorUnknown(ThoracicLymphNodePathologicScheme thoracicLymphNodePathologicScheme) {
        switch (thoracicLymphNodePathologicScheme){
            case TWO:
                return "N2";
            case THREE:
                return "N3";
            default:
                return "Nx";
        }
    }


//    private static boolean bothSides(List<LymphNode> lymphNodes, ThoracicLymphRegion thoracicLymphRegion) {
//        return lymphNodes.stream().filter(lymphNode -> lymphNode.getThoracicLymphRegion()
//                .equals(thoracicLymphRegion))
//                .map(LymphNode::getBodySide)
//                .collect(Collectors.toSet()).size() > 1;
//    }

    public List<LymphNode> getManualLymphNodes(List<String> lymphNodes) throws Exception {
        List<LymphNode> lymphNodeList = new ArrayList<>();
        for(String node : lymphNodes){
            if(node == null)
                continue;

            BodySide bodySide;
            String[] nodeSplit = node.split("-");
            switch (nodeSplit[nodeSplit.length-1]) {
                case "left":
                    bodySide = LEFT;
                    break;
                case "right":
                    bodySide = RIGHT;
                    break;
                default:
                    bodySide = BodySide.NONE;
            }
            lymphNodeList.add(new LymphNode(getLymphNodeStation(getCleanedUri(node)), bodySide, MANUAL));
        }
        return lymphNodeList;
    }

    private String getCleanedUri(String node) {
        return node.replace("-right","").replace("-left","");
    }


    private ThoracicLymphNodeStation getLymphNodeStation(String concept) throws Exception {
        String lowerCaseConcept = concept.toLowerCase();
        if(thoracicLymphNodeStationProperties.getOne().equals(lowerCaseConcept))
            return ONE;
        if(thoracicLymphNodeStationProperties.getTwo().equals(lowerCaseConcept))
            return TWO;
        if(thoracicLymphNodeStationProperties.getThreeA().equals(lowerCaseConcept))
            return THREE_A;
        if(thoracicLymphNodeStationProperties.getThreeP().equals(lowerCaseConcept))
            return THREE_P;
        if(thoracicLymphNodeStationProperties.getFour().equals(lowerCaseConcept))
            return FOUR;
        if(thoracicLymphNodeStationProperties.getFive().equals(lowerCaseConcept))
            return FIVE;
        if(thoracicLymphNodeStationProperties.getSix().equals(lowerCaseConcept))
            return SIX;
        if(thoracicLymphNodeStationProperties.getSeven().equals(lowerCaseConcept))
            return SEVEN;
        if(thoracicLymphNodeStationProperties.getEight().equals(lowerCaseConcept))
            return EIGHT;
        if(thoracicLymphNodeStationProperties.getNine().equals(lowerCaseConcept))
            return NINE;
        if(thoracicLymphNodeStationProperties.getTen().equals(lowerCaseConcept))
            return TEN;
        if(thoracicLymphNodeStationProperties.getEleven().equals(lowerCaseConcept))
            return ELEVEN;
        if(thoracicLymphNodeStationProperties.getTwelve().equals(lowerCaseConcept))
            return TWELVE;
        if(thoracicLymphNodeStationProperties.getThirteen().equals(lowerCaseConcept))
            return THIRTEEN;
        if(thoracicLymphNodeStationProperties.getFourteen().equals(lowerCaseConcept))
            return FOURTEEN;
        throw new Exception("Unknown Lymph Station: " + lowerCaseConcept);
    }

    private static ThoracicLymphNodePathologicScheme getThoracicLymphNodeScheme(LymphNode lymphNode){
        if(lymphNode.getThoracicLymphNodeStation() != null)
            return getThoracicLymphNodeScheme(lymphNode.getThoracicLymphNodeStation());
        else
            return ThoracicLymphNodePathologicScheme.UNKNOWN;
    }

    private static ThoracicLymphNodePathologicScheme getThoracicLymphNodeScheme(ThoracicLymphNodeStation thoracicLymphNodeStation){
        if(thoracicLymphNodeStation == null)
            return ThoracicLymphNodePathologicScheme.UNKNOWN;

        switch (thoracicLymphNodeStation) {
            case ONE:
                return ThoracicLymphNodePathologicScheme.THREE;
            case TWO:
                return ThoracicLymphNodePathologicScheme.DEFAULT;
            case THREE_A:
            case THREE_P:
                return ThoracicLymphNodePathologicScheme.TWO;
            case FOUR:
                return ThoracicLymphNodePathologicScheme.DEFAULT;
            case FIVE:
            case SIX:
                return ThoracicLymphNodePathologicScheme.AORTIC;
            case SEVEN:
                return ThoracicLymphNodePathologicScheme.TWO;
            case EIGHT:
            case NINE:
                return ThoracicLymphNodePathologicScheme.DEFAULT;
            case TEN:
            case ELEVEN:
            case TWELVE:
            case THIRTEEN:
            case FOURTEEN:
                return ThoracicLymphNodePathologicScheme.N1;
            default:
                return ThoracicLymphNodePathologicScheme.UNKNOWN;
        }
    }


}


