package com.github.putssander.medstruct.tnmclassifier.configuration.properties;


import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "lymph-node-concepts")
public class ThoracicLymphNodeConceptProperties {

    private String lymph = "snomedct:59441001";

    private String lymphNodeSize = "none:lymph-node-size";

    private String lymphPathologicAdj = "none:lymph-nodes-pathologic-adj";

    private String lymphAdenopathy = "none:lymphadenopathy";

    public String getLymph() {
        return lymph;
    }

    public void setLymph(String lymph) {
        this.lymph = lymph;
    }

    public String getLymphNodeSize() {
        return lymphNodeSize;
    }

    public void setLymphNodeSize(String lymphNodeSize) {
        this.lymphNodeSize = lymphNodeSize;
    }

    public String getLymphPathologicAdj() {
        return lymphPathologicAdj;
    }

    public void setLymphPathologicAdj(String lymphPathologicAdj) {
        this.lymphPathologicAdj = lymphPathologicAdj;
    }

    public String getLymphAdenopathy() {
        return lymphAdenopathy;
    }

    public void setLymphAdenopathy(String lymphAdenopathy) {
        this.lymphAdenopathy = lymphAdenopathy;
    }
}
