package com.github.putssander.medstruct.tnmclassifier.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ConfigurationProperties("lung-tnm-classification.context")
public class ContextProperties {

    private List<String> exist = Arrays.asList(new String[]{"definite_existence", "historical"});
    private List<String> negated = Arrays.asList(new String[]{"definite_negated_existence", "pseudoneg"});
    private List<String> uncertain = Arrays.asList(new String[]{"probable_existence", "probable_negated_existence", "indication, ambivalent_existence"});
    private List<String> historical = Arrays.asList(new String[]{"historical"});

    private List<String> existConceptBlacklistModifiers = Arrays.asList(new String[]{"limited_amount"});
    private List<String> existConceptBlacklist = new ArrayList<>();

    private List<String> limitedAmount = Arrays.asList(new String[]{"limited_amount"});
    private List<String> limitedAvidity = Arrays.asList(new String[]{"limited_avidity"});
    private List<String> substantialAvidity = Arrays.asList(new String[]{"substantial_avidity"});


    public List<String> getExist() {
        return exist;
    }

    public void setExist(List<String> exist) {
        this.exist = exist;
    }

    public List<String> getNegated() {
        return negated;
    }

    public void setNegated(List<String> negated) {
        this.negated = negated;
    }

    public List<String> getUncertain() {
        return uncertain;
    }

    public void setUncertain(List<String> uncertain) {
        this.uncertain = uncertain;
    }

    public List<String> getExistConceptBlacklistModifiers() {
        return existConceptBlacklistModifiers;
    }

    public void setExistConceptBlacklistModifiers(List<String> existConceptBlacklistModifiers) {
        this.existConceptBlacklistModifiers = existConceptBlacklistModifiers;
    }

    public List<String> getExistConceptBlacklist() {
        return existConceptBlacklist;
    }

    public void setExistConceptBlacklist(List<String> existConceptBlacklist) {
        this.existConceptBlacklist = existConceptBlacklist;
    }

    public List<String> getHistorical() {
        return historical;
    }

    public void setHistorical(List<String> historical) {
        this.historical = historical;
    }

    public List<String> getLimitedAvidity() {
        return limitedAvidity;
    }

    public void setLimitedAvidity(List<String> limitedAvidity) {
        this.limitedAvidity = limitedAvidity;
    }

    public List<String> getSubstantialAvidity() {
        return substantialAvidity;
    }

    public void setSubstantialAvidity(List<String> substantialAvidity) {
        this.substantialAvidity = substantialAvidity;
    }

    public List<String> getLimitedAmount() {
        return limitedAmount;
    }

    public void setLimitedAmount(List<String> limitedAmount) {
        this.limitedAmount = limitedAmount;
    }
}
