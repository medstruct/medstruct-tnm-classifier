package com.github.putssander.medstruct.tnmclassifier.classification;


import com.github.putssander.medstruct.tnmclassifier.constants.BodySide;

import java.util.ArrayList;
import java.util.List;

public class TnmClassificationRequest {

    private BodySide tumorSide;

    private double tumorSize;

    private List<String> presentConcepts = new ArrayList<>();

    private List<String> involvedConcepts = new ArrayList<>();

    private boolean satelliteNodules;

    private boolean tumorsInDiffentLobesIpsilateral;

    private List<String> lymphNodes = new ArrayList<>();

    public BodySide getTumorSide() {
        return tumorSide;
    }

    public void setTumorSide(BodySide tumorSide) {
        this.tumorSide = tumorSide;
    }

    public double getTumorSize() {
        return tumorSize;
    }

    public void setTumorSize(double tumorSize) {
        this.tumorSize = tumorSize;
    }

    public List<String> getPresentConcepts() {
        return presentConcepts;
    }

    public void setPresentConcepts(List<String> presentConcepts) {
        this.presentConcepts = presentConcepts;
    }

    public List<String> getInvolvedConcepts() {
        return involvedConcepts;
    }

    public void setInvolvedConcepts(List<String> involvedConcepts) {
        this.involvedConcepts = involvedConcepts;
    }

    public boolean isSatelliteNodules() {
        return satelliteNodules;
    }

    public void setSatelliteNodules(boolean satelliteNodules) {
        this.satelliteNodules = satelliteNodules;
    }

    public boolean isTumorsInDiffentLobesIpsilateral() {
        return tumorsInDiffentLobesIpsilateral;
    }

    public void setTumorsInDiffentLobesIpsilateral(boolean tumorsInDiffentLobesIpsilateral) {
        this.tumorsInDiffentLobesIpsilateral = tumorsInDiffentLobesIpsilateral;
    }

    public List<String> getLymphNodes() {
        return lymphNodes;
    }

    public void setLymphNodes(List<String> lymphNodes) {
        this.lymphNodes = lymphNodes;
    }
}
