package com.github.putssander.medstruct.tnmclassifier.service;

import com.github.putssander.medstruct.tnmclassifier.classification.MeasurementExpression;
import com.github.putssander.medstruct.tnmclassifier.configuration.properties.TnmProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@EnableConfigurationProperties(TnmProperties.class)
public class MedStructService {


    public static List<MeasurementExpression> getMeasurementDimensionWithoutCode(List<MeasurementExpression> measurements) {
        return measurements.stream()
                .filter(measurement ->
                        measurement.getCode() == null
                                && measurement.getQuantitySubtype() != null
                                && measurement.getQuantitySubtype().equals("DIMENSION"))
                .collect(Collectors.toList());
    }

    public static List<MeasurementExpression> getMeasurementLengthWithoutCodeAndSubtype(List<MeasurementExpression> measurements) {
        return measurements.stream()
                .filter(measurement ->
                        measurement.getCode() == null
                                && measurement.getQuantitySubtype() == null
                                && measurement.getQuantity() != null
                                && measurement.getQuantity().equals("LENGTH"))
                .collect(Collectors.toList());
    }

    public static List<BigDecimal> getMeasurementCodeValues(List<MeasurementExpression> measurements){
        return measurements.stream().map(MeasurementExpression::getCodeValue).collect(Collectors.toList());
    }

    public static Optional<BigDecimal> getMaxMeasurement(List<MeasurementExpression> measurementExpressions, Integer upperLimit){
        List<BigDecimal> list = measurementExpressions.stream()
                .map(MeasurementExpression::getNormalizedValues)
                .collect(Collectors.toList()).stream().flatMap(List::stream)
                .collect(Collectors.toList());

        if(upperLimit != null)
            list = list.stream().filter(item -> item.longValue() <= upperLimit).collect(Collectors.toList());
        return list.stream().parallel().max(Comparator.naturalOrder());
    }

}
