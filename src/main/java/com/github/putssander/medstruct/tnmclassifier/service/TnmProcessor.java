package com.github.putssander.medstruct.tnmclassifier.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.integration.annotation.Transformer;
import org.springframework.stereotype.Service;


@Service
@EnableBinding(Processor.class)
@ConditionalOnProperty(prefix="processor", name="enabled")
public class TnmProcessor {

    private static final Logger logger = LoggerFactory.getLogger(TnmProcessor.class);

    private TnmClassificationService tnmClassificationService;

    public TnmProcessor(TnmClassificationService tnmClassificationService) {
        logger.info("processor.enabled=true");
        this.tnmClassificationService = tnmClassificationService;
    }

    @Transformer(inputChannel = Processor.INPUT, outputChannel = Processor.OUTPUT)
    public String handle(String object) throws Exception {
        logger.info("INPUT");
        String out = tnmClassificationService.classify(object);
        logger.info("OUTPUT");
        return out;
    }

}