package com.github.putssander.medstruct.tnmclassifier.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.putssander.medstruct.tnmclassifier.classification.BodySideMention;
import com.github.putssander.medstruct.tnmclassifier.configuration.properties.TnmProperties;
import com.github.putssander.medstruct.tnmclassifier.constants.BodySide;
import com.github.putssander.medstruct.tnmclassifier.util.JsonNlpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@EnableConfigurationProperties(TnmProperties.class)
public class BodySideService {

    private static final Logger logger = LoggerFactory.getLogger(BodySideService.class);

    private TnmProperties tnmProperties;

    public BodySideService(TnmProperties tnmProperties) {
        this.tnmProperties = tnmProperties;
    }


    public static BodySideMention getTokenBodySideMention(List<Integer> bodySideRightTokenIds,
                                                          List<Integer> bodySideLeftTokenIds,
                                                          JsonNode document,
                                                          List<Integer> tokens){

        Collections.sort(bodySideRightTokenIds);
        Collections.sort(bodySideLeftTokenIds);
        Collections.sort(tokens);

        if(tokens.isEmpty())
            return new BodySideMention(BodySide.UNKNOWN, -1);

        BodySideMention bodySide;
        int conceptToken = tokens.get(tokens.size() - 1);
        logger.debug("Extract body side for concept token {}", conceptToken);
        int forwardPunctuationEnd = JsonNlpUtil.getFirstPunctuationIdForward(document, conceptToken);

        int sentenceId = JsonNlpUtil.getSentenceId(document, conceptToken);
        int sentenceTokenFrom = JsonNlpUtil.getSentenceTokenFrom(document, sentenceId);
        int sentenceTokenTo = JsonNlpUtil.getSentenceTokenTo(document, sentenceId) - 1;

        //try forward sentence_clause
        logger.debug("bodyside forward sentence clause");
        bodySide = getBodySideForwards(bodySideRightTokenIds, bodySideLeftTokenIds, conceptToken, forwardPunctuationEnd);
        if(bodySide != null) return bodySide;

        //try backwards sentence
        logger.debug("bodyside backwards sentence");
        bodySide = getBodySideBackwards(bodySideRightTokenIds, bodySideLeftTokenIds, conceptToken, sentenceTokenFrom);
        if(bodySide != null) return bodySide;

        //try forward sentence
        logger.debug("bodyside forward sentence");
        bodySide = getBodySideForwards(bodySideRightTokenIds, bodySideLeftTokenIds, forwardPunctuationEnd, sentenceTokenTo);
        if(bodySide != null) return bodySide;

        //try backwards document
        logger.debug("bodyside backwards documents");
        bodySide = getBodySideBackwards(bodySideRightTokenIds, bodySideLeftTokenIds, sentenceTokenFrom, 1);
        if(bodySide != null) return bodySide;

        //try forwards next sentence
        logger.debug("bodyside next sentence");
        Integer forwardEnd = getTokenToNextSentence(document, tokens.get(0));
        bodySide = getBodySideForwards(bodySideRightTokenIds, bodySideLeftTokenIds, sentenceTokenTo, forwardEnd);
        if(bodySide != null) return bodySide;

        return null;
    }

    private static BodySideMention getBodySideForwards(List<Integer> bodySideRightTokenIds, List<Integer> bodySideLeftTokenIds, int tokenIdMin, int tokenIdMax){
        for(int i = tokenIdMin; i < tokenIdMax; i++){
            if (bodySideRightTokenIds.contains(i))
                return new BodySideMention(BodySide.RIGHT, i);
            if (bodySideLeftTokenIds.contains(i))
                return new BodySideMention(BodySide.LEFT, i);
        }
        return null;
    }

    private static BodySideMention getBodySideBackwards(List<Integer> bodySideRightTokenIds, List<Integer> bodySideLeftTokenIds, int tokenIdMax, int tokenIdMin){
        for(int i = tokenIdMax; i >= tokenIdMin; i--) {
            if (bodySideRightTokenIds.contains(i))
                return new BodySideMention(BodySide.RIGHT, i);
            if (bodySideLeftTokenIds.contains(i))
                return new BodySideMention(BodySide.LEFT, i);
        }
        return null;
    }


    protected List<Integer> getBodySideLeftTokenIds(JsonNode document) {
        List<Integer> bodySideTokenIds = new ArrayList<>();
        JsonNlpUtil.getTokensForUri(document, tnmProperties.getBodySideLeftUri())
                .stream().forEach(token -> bodySideTokenIds.add(token.get("id").asInt()));
        JsonNlpUtil.getTokensForUri(document, tnmProperties.getConceptSuperiorLobeLeft())
                .stream().forEach(token -> bodySideTokenIds.add(token.get("id").asInt()));
        JsonNlpUtil.getTokensForUri(document, tnmProperties.getConceptInferiorLobeLeft())
                .stream().forEach(token -> bodySideTokenIds.add(token.get("id").asInt()));
        return bodySideTokenIds;
    }

    protected List<Integer> getBodySideRightTokenIds(JsonNode document) {
        List<Integer> bodySideTokenIds = new ArrayList<>();
        JsonNlpUtil.getTokensForUri(document, tnmProperties.getBodySideRightUri())
                .stream().forEach(token -> bodySideTokenIds.add(token.get("id").asInt()));
        JsonNlpUtil.getTokensForUri(document, tnmProperties.getConceptSuperiorLobeRight())
                .stream().forEach(token -> bodySideTokenIds.add(token.get("id").asInt()));
        JsonNlpUtil.getTokensForUri(document, tnmProperties.getConceptMiddleLobe())
                .stream().forEach(token -> bodySideTokenIds.add(token.get("id").asInt()));
        JsonNlpUtil.getTokensForUri(document, tnmProperties.getConceptInferiorLobeRight())
                .stream().forEach(token -> bodySideTokenIds.add(token.get("id").asInt()));
        return bodySideTokenIds;
    }


    /**
     * look max 1 sentence ahead
     * @param document
     * @param sentenceToken
     * @return
     */
    private static Integer getTokenToNextSentence(JsonNode document, Integer sentenceToken){
        Integer sentenceId = JsonNlpUtil.getSentenceId(JsonNlpUtil.getTokenById(document, sentenceToken));
        Integer documentEnd = JsonNlpUtil.getDocumentTokensEnd(document);
        Integer sentenceEnd  = JsonNlpUtil.getSentence(document, sentenceId).get("tokenTo").asInt() - 1;
        if(sentenceEnd.equals(documentEnd))
            return sentenceEnd;
        else
            return JsonNlpUtil.getSentence(document, sentenceId + 1).get("tokenTo").asInt() - 1;
    }

}
