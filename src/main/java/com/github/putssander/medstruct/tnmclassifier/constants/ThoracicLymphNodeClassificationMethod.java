package com.github.putssander.medstruct.tnmclassifier.constants;

public enum ThoracicLymphNodeClassificationMethod {
    UNKNOWN, MANUAL, PATHOLOGIC, SIZE, AVIDE
}
