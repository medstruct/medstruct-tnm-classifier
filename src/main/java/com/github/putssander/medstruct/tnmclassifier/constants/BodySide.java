package com.github.putssander.medstruct.tnmclassifier.constants;

public enum BodySide {

    LEFT, RIGHT, NONE, UNKNOWN
}
