package com.github.putssander.medstruct.tnmclassifier.configuration.properties;


import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.*;

@ConfigurationProperties("lung-tnm-classification")
public class TnmProperties {

    private String conceptInvolvement;

    private String conceptTumor;

    private String conceptTumorSize;

    private String conceptLymphNodeSize;

    private boolean unknownSizeIsTumorSize = true;

    private String lungSizeBlacklistPattern;

    private boolean applyLungSizeBlacklistPattern = false;

    private String secondaryTumorBlacklistPattern;

    private boolean applySecondaryTumorBlacklistPattern = false;

    private double minTumorSize;

    private List<String> conceptTumorBlackList = new ArrayList<>();

    private String conceptLymphNodes;

    private List<String> conceptsInvolvementT1 = new ArrayList<>();

    private List<String> conceptsInvolvementT2 = new ArrayList<>();

    private List<String> conceptsInvolvementT3 = new ArrayList<>();

    private List<String> conceptsInvolvementT4 = new ArrayList<>();

    private List<String> conceptsPresentT1 = new ArrayList<>();

    private List<String> conceptsPresentT2 = new ArrayList<>();

    private List<String> conceptsPresentT3 = new ArrayList<>();

    private List<String> conceptsPresentT4 = new ArrayList<>();

    private TreeMap<String, Double> sizeLabelLargerThan = new TreeMap<>();

    private String conceptSuperiorLobeRight;

    private String conceptSuperiorLobeLeft;

    private String conceptMiddleLobe;

    private String conceptInferiorLobeRight;

    private String conceptInferiorLobeLeft;

    private int maxValidTumorSizeCm = 40;

    private String conceptSatellite;

    private String bodySideRightUri = "snomedct:85421007";

    private String bodySideLeftUri = "snomedct:31156008";

    private String avide = "none:avide";

    public String getConceptInvolvement() {
        return conceptInvolvement;
    }

    public void setConceptInvolvement(String conceptInvolvement) {
        this.conceptInvolvement = conceptInvolvement;
    }

    public String getConceptTumor() {
        return conceptTumor;
    }

    public void setConceptTumor(String conceptTumor) {
        this.conceptTumor = conceptTumor;
    }

    public String getConceptTumorSize() {
        return conceptTumorSize;
    }

    public void setConceptTumorSize(String conceptTumorSize) {
        this.conceptTumorSize = conceptTumorSize;
    }

    public String getConceptLymphNodeSize() {
        return conceptLymphNodeSize;
    }

    public void setConceptLymphNodeSize(String conceptLymphNodeSize) {
        this.conceptLymphNodeSize = conceptLymphNodeSize;
    }

    public boolean isUnknownSizeIsTumorSize() {
        return unknownSizeIsTumorSize;
    }

    public void setUnknownSizeIsTumorSize(boolean unknownSizeIsTumorSize) {
        this.unknownSizeIsTumorSize = unknownSizeIsTumorSize;
    }



    public String getSecondaryTumorBlacklistPattern() {
        return secondaryTumorBlacklistPattern;
    }

    public void setSecondaryTumorBlacklistPattern(String secondaryTumorBlacklistPattern) {
        this.secondaryTumorBlacklistPattern = secondaryTumorBlacklistPattern;
    }

    public List<String> getConceptTumorBlackList() {
        return conceptTumorBlackList;
    }

    public void setConceptTumorBlackList(List<String> conceptTumorBlackList) {
        this.conceptTumorBlackList = conceptTumorBlackList;
    }

    public String getConceptLymphNodes() {
        return conceptLymphNodes;
    }

    public void setConceptLymphNodes(String conceptLymphNodes) {
        this.conceptLymphNodes = conceptLymphNodes;
    }

    public List<String> getConceptsInvolvementT1() {
        return conceptsInvolvementT1;
    }

    public void setConceptsInvolvementT1(List<String> conceptsInvolvementT1) {
        this.conceptsInvolvementT1 = conceptsInvolvementT1;
    }

    public List<String> getConceptsInvolvementT2() {
        return conceptsInvolvementT2;
    }

    public void setConceptsInvolvementT2(List<String> conceptsInvolvementT2) {
        this.conceptsInvolvementT2 = conceptsInvolvementT2;
    }

    public List<String> getConceptsInvolvementT3() {
        return conceptsInvolvementT3;
    }

    public void setConceptsInvolvementT3(List<String> conceptsInvolvementT3) {
        this.conceptsInvolvementT3 = conceptsInvolvementT3;
    }

    public List<String> getConceptsInvolvementT4() {
        return conceptsInvolvementT4;
    }

    public void setConceptsInvolvementT4(List<String> conceptsInvolvementT4) {
        this.conceptsInvolvementT4 = conceptsInvolvementT4;
    }

    public List<String> getConceptsPresentT1() {
        return conceptsPresentT1;
    }

    public void setConceptsPresentT1(List<String> conceptsPresentT1) {
        this.conceptsPresentT1 = conceptsPresentT1;
    }

    public List<String> getConceptsPresentT2() {
        return conceptsPresentT2;
    }

    public void setConceptsPresentT2(List<String> conceptsPresentT2) {
        this.conceptsPresentT2 = conceptsPresentT2;
    }

    public List<String> getConceptsPresentT3() {
        return conceptsPresentT3;
    }

    public void setConceptsPresentT3(List<String> conceptsPresentT3) {
        this.conceptsPresentT3 = conceptsPresentT3;
    }

    public List<String> getConceptsPresentT4() {
        return conceptsPresentT4;
    }

    public void setConceptsPresentT4(List<String> conceptsPresentT4) {
        this.conceptsPresentT4 = conceptsPresentT4;
    }

    public TreeMap<String, Double> getSizeLabelLargerThan() {
        return sizeLabelLargerThan;
    }

    public void setSizeLabelLargerThan(TreeMap<String, Double> sizeLabelLargerThan) {
        this.sizeLabelLargerThan = sizeLabelLargerThan;
    }

    public String getConceptSuperiorLobeRight() {
        return conceptSuperiorLobeRight;
    }

    public void setConceptSuperiorLobeRight(String conceptSuperiorLobeRight) {
        this.conceptSuperiorLobeRight = conceptSuperiorLobeRight;
    }

    public String getConceptSuperiorLobeLeft() {
        return conceptSuperiorLobeLeft;
    }

    public void setConceptSuperiorLobeLeft(String conceptSuperiorLobeLeft) {
        this.conceptSuperiorLobeLeft = conceptSuperiorLobeLeft;
    }

    public String getConceptMiddleLobe() {
        return conceptMiddleLobe;
    }

    public void setConceptMiddleLobe(String conceptMiddleLobe) {
        this.conceptMiddleLobe = conceptMiddleLobe;
    }

    public String getConceptInferiorLobeRight() {
        return conceptInferiorLobeRight;
    }

    public void setConceptInferiorLobeRight(String conceptInferiorLobeRight) {
        this.conceptInferiorLobeRight = conceptInferiorLobeRight;
    }

    public String getConceptInferiorLobeLeft() {
        return conceptInferiorLobeLeft;
    }

    public void setConceptInferiorLobeLeft(String conceptInferiorLobeLeft) {
        this.conceptInferiorLobeLeft = conceptInferiorLobeLeft;
    }

    public int getMaxValidTumorSizeCm() {
        return maxValidTumorSizeCm;
    }

    public void setMaxValidTumorSizeCm(int maxValidTumorSizeCm) {
        this.maxValidTumorSizeCm = maxValidTumorSizeCm;
    }

    public String getConceptSatellite() {
        return conceptSatellite;
    }

    public void setConceptSatellite(String conceptSatellite) {
        this.conceptSatellite = conceptSatellite;
    }

    public boolean isApplySecondaryTumorBlacklistPattern() {
        return applySecondaryTumorBlacklistPattern;
    }

    public void setApplySecondaryTumorBlacklistPattern(boolean applySecondaryTumorBlacklistPattern) {
        this.applySecondaryTumorBlacklistPattern = applySecondaryTumorBlacklistPattern;
    }

    public double getMinTumorSize() {
        return minTumorSize;
    }

    public void setMinTumorSize(double minTumorSize) {
        this.minTumorSize = minTumorSize;
    }

    public String getLungSizeBlacklistPattern() {
        return lungSizeBlacklistPattern;
    }

    public void setLungSizeBlacklistPattern(String lungSizeBlacklistPattern) {
        this.lungSizeBlacklistPattern = lungSizeBlacklistPattern;
    }

    public boolean isApplyLungSizeBlacklistPattern() {
        return applyLungSizeBlacklistPattern;
    }

    public void setApplyLungSizeBlacklistPattern(boolean applyLungSizeBlacklistPattern) {
        this.applyLungSizeBlacklistPattern = applyLungSizeBlacklistPattern;
    }

    public String getBodySideRightUri() {
        return bodySideRightUri;
    }

    public void setBodySideRightUri(String bodySideRightUri) {
        this.bodySideRightUri = bodySideRightUri;
    }

    public String getBodySideLeftUri() {
        return bodySideLeftUri;
    }

    public void setBodySideLeftUri(String bodySideLeftUri) {
        this.bodySideLeftUri = bodySideLeftUri;
    }

    public String getAvide() {
        return avide;
    }

    public void setAvide(String avide) {
        this.avide = avide;
    }
}
