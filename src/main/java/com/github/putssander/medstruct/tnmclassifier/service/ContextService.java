package com.github.putssander.medstruct.tnmclassifier.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.putssander.medstruct.tnmclassifier.classification.ConceptItem;
import com.github.putssander.medstruct.tnmclassifier.configuration.properties.ContextProperties;
import com.github.putssander.medstruct.tnmclassifier.util.JsonNlpUtil;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static com.github.putssander.medstruct.tnmclassifier.constants.MedStructConstants.*;
import static com.github.putssander.medstruct.tnmclassifier.util.JsonNlpUtil.getContextTargetUri;

@Service
@EnableConfigurationProperties({ContextProperties.class})
public class ContextService {


    private ContextProperties contextProperties;

    private ObjectMapper objectMapper;


    public ContextService(ContextProperties contextProperties, ObjectMapper objectMapper) {
        this.contextProperties = contextProperties;
        this.objectMapper = objectMapper;
    }


    public boolean isPresent(JsonNode contextElement){

        Iterator<JsonNode> modifiers = contextElement.get(LAYER_CONTEXT_MODIFIERS).elements();
        if(!modifiers.hasNext()){
            return true;
        }

        boolean foundExistModifier = false;
        boolean foundExistBlacklistModifier = false;
        boolean foundUncertainModifier = false;
        boolean foundNegationModifier = false;
        while (modifiers.hasNext()){
            JsonNode modifier = modifiers.next();
            if(contextProperties.getExist().contains(modifier.get(LAYER_CONTEXT_CATEGORY).get(0).textValue())){
                foundExistModifier = true;
            }
            if(contextProperties.getExistConceptBlacklistModifiers().contains(modifier.get(LAYER_CONTEXT_CATEGORY).get(0).textValue())){
                foundExistBlacklistModifier = true;
            }
            if(contextProperties.getUncertain().contains(modifier.get(LAYER_CONTEXT_CATEGORY).get(0).textValue())){
                foundUncertainModifier = true;
            }
            if(contextProperties.getNegated().contains(modifier.get(LAYER_CONTEXT_CATEGORY).get(0).textValue())){
                foundNegationModifier = true;
            }
        }

        if(foundExistBlacklistModifier && contextProperties.getExistConceptBlacklist()
                .contains(getContextTargetUri(contextElement))) {
            return false;
        }

        return !foundNegationModifier && !foundUncertainModifier;
    }


    public List<ConceptItem> getConceptNotPresentItems(JsonNode document, String uri){
        return getContextElements(document, uri).stream()
                .filter(c -> isModifiedByContextType(c, contextProperties.getNegated()))
                .map(c -> getContextItem(c, document, uri))
                .collect(Collectors.toList());
    }

    public Collection<? extends ConceptItem> isLimitedAmount(JsonNode document, String uri) {
        return getContextElements(document, uri).stream()
                .filter(c -> isModifiedByContextType(c, contextProperties.getLimitedAmount()))
                .map(c -> getContextItem(c, document, uri))
                .collect(Collectors.toList());
    }

    public List<ConceptItem> isSubstantialAvidity(JsonNode document, String uri){
        return getContextElements(document, uri).stream()
                .filter(c -> isModifiedByContextType(c, contextProperties.getSubstantialAvidity()))
                .filter(c -> !isModifiedByContextType(c, contextProperties.getNegated()))
                .map(c -> getContextItem(c, document, uri))
                .collect(Collectors.toList());
    }

    public List<ConceptItem> isLimitedAvidity(JsonNode document, String uri){
        return getContextElements(document, uri).stream()
                .filter(c -> isModifiedByContextType(c, contextProperties.getLimitedAvidity()))
                .map(c -> getContextItem(c, document, uri))
                .collect(Collectors.toList());
    }

    public boolean isModifiedByContextType(JsonNode contextElement, List<String> contextTypeList){
        Iterator<JsonNode> modifiers = contextElement.get(LAYER_CONTEXT_MODIFIERS).elements();
        while (modifiers.hasNext()){
            JsonNode modifier = modifiers.next();
            if(contextTypeList.contains(modifier.get(LAYER_CONTEXT_CATEGORY).get(0).textValue())){
                return true;
            }
        }
        return false;
    }

    public List<ConceptItem> getConceptPresentItems(JsonNode document, String uri){
        return getContextElements(document, uri).stream()
                .filter(c -> isPresent(c))
                .map(c -> getContextItem(c, document, uri))
                .collect(Collectors.toList());
    }

    public List<JsonNode> getContextElements(JsonNode document, String uri){
        Iterator<JsonNode> contextElements = document.get(LAYER_CONTEXT).elements();
        List<JsonNode> filteredContextElements = new ArrayList<>();
        while (contextElements.hasNext()){
            JsonNode contextElement = contextElements.next();
            if(contextElement.get(LAYER_CONTEXT_TARGET).get(LAYER_CONTEXT_CATEGORY).get(0).textValue().equals(uri)){
                filteredContextElements.add(contextElement);
            }
        }
        return filteredContextElements;
    }

    private ConceptItem getContextItem(JsonNode contextElement, JsonNode document, String uri){
        List<Integer> tokens = objectMapper.convertValue(contextElement.get(LAYER_CONTEXT_TARGET).get(LAYER_CONTEXT_TOKENS), ArrayList.class);
        return JsonNlpUtil.getTokenToConceptItem(document, uri, tokens);
    }

}
