package com.github.putssander.medstruct.tnmclassifier.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.putssander.medstruct.tnmclassifier.classification.MeasurementExpression;
import com.github.putssander.medstruct.tnmclassifier.classification.Tumor;
import com.github.putssander.medstruct.tnmclassifier.constants.BodySide;
import com.github.putssander.medstruct.tnmclassifier.constants.LungLobe;
import com.github.putssander.medstruct.tnmclassifier.configuration.properties.TnmProperties;
import com.github.putssander.medstruct.tnmclassifier.util.JsonNlpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

import static com.github.putssander.medstruct.tnmclassifier.constants.BodySide.LEFT;
import static com.github.putssander.medstruct.tnmclassifier.constants.BodySide.RIGHT;
import static com.github.putssander.medstruct.tnmclassifier.constants.LungLobe.*;


public class MultipleTumorIpsilateralLobeService {

    private static final Logger logger = LoggerFactory.getLogger(MultipleTumorIpsilateralLobeService.class);


    public static List<Tumor> getTumors(TnmProperties tnmProperties,
                                        JsonNode document,
                                        List<MeasurementExpression> measurements){

        List<Integer> tumorSentences = JsonNlpUtil.getUriSentenceIds(document, tnmProperties.getConceptTumor());
        List<Tumor> tumors = new ArrayList<>();

        for(int tumorSentence : tumorSentences){

            JsonNode sentence = JsonNlpUtil.getSentence(document, tumorSentence);
            int sentenceBegin = sentence.get("tokenFrom").asInt();
            int sentenceEnd = sentence.get("tokenTo").asInt();

            int sentenceMeasurementIndex = 0;
            List<MeasurementExpression> sentenceMeasurements = getSentenceMeasurements(measurements, sentenceBegin, sentenceEnd);

            boolean tumorMention = true;
            List<JsonNode> sentenceTokens = JsonNlpUtil.getTokensForSentenceId(document, tumorSentence);

            for(JsonNode token : sentenceTokens) {

                Tumor tumor = null;

                if(sentenceMeasurementIndex >= sentenceMeasurements.size())
                    break;

                try {
                    double currentSize = Collections.max(sentenceMeasurements.get(sentenceMeasurementIndex).getNormalizedValues()).doubleValue();
                    String currentUnit = sentenceMeasurements.get(sentenceMeasurementIndex).getNormalizedUnit();
                    List<String> uris = JsonNlpUtil.getTokenUris(token);
                    if (!isTokenAConceptBegin(token, uris)) continue;

                    String uri = uris.get(0);
                    if (tnmProperties.getConceptTumor().equals(uri)) {
                        tumorMention = true;
                        continue;
                    }
                    if (tnmProperties.getConceptLymphNodes().equals(uri)) {
                        tumorMention = false;
                        continue;
                    }

                    if (!tumorMention)
                        continue;

                    if (tnmProperties.getConceptSuperiorLobeLeft().equals(uri))
                        tumor = new Tumor(currentSize, currentUnit, LEFT, LUL, tumorSentence);
                    else if (tnmProperties.getConceptInferiorLobeLeft().equals(uri))
                        tumor = new Tumor(currentSize, currentUnit, LEFT, LLL, tumorSentence);
                    else if (tnmProperties.getConceptSuperiorLobeRight().equals(uri))
                        tumor = new Tumor(currentSize, currentUnit, RIGHT, RUL, tumorSentence);
                    else if (tnmProperties.getConceptMiddleLobe().equals(uri))
                        tumor = new Tumor(currentSize, currentUnit, RIGHT, RML, tumorSentence);
                    else if (tnmProperties.getConceptInferiorLobeRight().equals(uri))
                        tumor = new Tumor(currentSize, currentUnit, RIGHT, RLL, tumorSentence);
                }
                catch (Exception e){
                    logger.error("Unable to parse measurement", e);
                    sentenceMeasurementIndex++;
                }

                if(tumor != null && !tumors.contains(tumor)) {
                    sentenceMeasurementIndex++;
                    tumors.add(tumor);
                }
            }
        }

        return tumors;
    }

    private static boolean isTokenAConceptBegin(JsonNode token, List<String> uris) {
        return !uris.isEmpty() && token.get("entity_iob").textValue().equals("B");
    }

    private static List<MeasurementExpression> getSentenceMeasurements(List<MeasurementExpression> measurements, int sentenceBegin, int sentenceEnd) {
        return measurements.stream().filter(
                measurementExpression ->
                        (sentenceBegin <= measurementExpression.getTokens().get(0))
                                && (measurementExpression.getTokens().get(0) < sentenceEnd))
                .collect(Collectors.toList());
    }


    public static boolean isIpsilateralTumors(JsonNode document, TnmProperties tnmProperties, List<Tumor> tumors, double minTumorSize){
        Set<LungLobe> lobesLeft = new HashSet<>();
        Set<LungLobe> lobesRight = new HashSet<>();
        tumors.forEach(tumor -> extendIpsilateralTumorLists(tnmProperties, document, tumor, lobesLeft, lobesRight));
        return lobesLeft.size() > 1 || lobesRight.size() > 1;
    }

    private static void extendIpsilateralTumorLists(TnmProperties tnmProperties, JsonNode document, Tumor tumor, Set<LungLobe> lobesLeft, Set<LungLobe> lobesRight) {

        List<JsonNode> sentenceTokens = JsonNlpUtil.getTokensForSentenceId(document, tumor.getSentenceId());
        if (tnmProperties.isApplySecondaryTumorBlacklistPattern() && sentenceContainsSecondyTumorBlackListPattern(tnmProperties, sentenceTokens)) {
            return;
        }

        if (tumor.getSize() > tnmProperties.getMinTumorSize()) {
            if (tumor.getBodySide().equals(BodySide.LEFT))
                lobesLeft.add(tumor.getLungLobe());
            if (tumor.getBodySide().equals(BodySide.RIGHT))
                lobesRight.add(tumor.getLungLobe());
        }
    }

    public static boolean sentenceContainsSecondyTumorBlackListPattern(TnmProperties tnmProperties, List<JsonNode> sentenceTokens) {
        for (JsonNode token : sentenceTokens) {
            if (token.get("text").textValue().matches(tnmProperties.getSecondaryTumorBlacklistPattern())) {
                return true;
            }
        }
        return false;
    }

}
