package com.github.putssander.medstruct.tnmclassifier;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TnmClassifier {

	public static void main(String[] args) {
		SpringApplication.run(TnmClassifier.class, args);
	}

}
