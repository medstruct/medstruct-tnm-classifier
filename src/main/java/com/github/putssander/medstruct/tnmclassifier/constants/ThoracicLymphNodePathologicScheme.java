package com.github.putssander.medstruct.tnmclassifier.constants;

/**
 * Measurements are required: min lymph-size is 1 cm
 * N classification 5 schemes
 * - DEFAULT: CONTRA 3 IPS 2
 * - N1: CONTRA 3 IPS 1
 * - AORTIC: TUMOR LEFT 2 TUMOR RIGHT 3
 * - TWO: Always 2
 * - THREE: Always 3
 **/
public enum ThoracicLymphNodePathologicScheme {
    UNKNOWN, N1, TWO, THREE, DEFAULT, AORTIC
}
