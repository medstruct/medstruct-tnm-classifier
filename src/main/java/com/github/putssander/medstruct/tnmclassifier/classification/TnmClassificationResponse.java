package com.github.putssander.medstruct.tnmclassifier.classification;

public class TnmClassificationResponse {

    private boolean tnmT1;

    private boolean tnmT2;

    private boolean tnmT3;

    private boolean tnmT4;

    private String tnmT;

    private String tnmN;

    private String tnmM;

    private String tnmSize;

    public boolean isTnmT1() {
        return tnmT1;
    }

    public void setTnmT1(boolean tnmT1) {
        this.tnmT1 = tnmT1;
    }

    public boolean isTnmT2() {
        return tnmT2;
    }

    public void setTnmT2(boolean tnmT2) {
        this.tnmT2 = tnmT2;
    }

    public boolean isTnmT3() {
        return tnmT3;
    }

    public void setTnmT3(boolean tnmT3) {
        this.tnmT3 = tnmT3;
    }

    public boolean isTnmT4() {
        return tnmT4;
    }

    public void setTnmT4(boolean tnmT4) {
        this.tnmT4 = tnmT4;
    }

    public String getTnmT() {
        return tnmT;
    }

    public void setTnmT(String tnmT) {
        this.tnmT = tnmT;
    }

    public String getTnmN() {
        return tnmN;
    }

    public void setTnmN(String tnmN) {
        this.tnmN = tnmN;
    }

    public String getTnmM() {
        return tnmM;
    }

    public void setTnmM(String tnmM) {
        this.tnmM = tnmM;
    }

    public String getTnmSize() {
        return tnmSize;
    }

    public void setTnmSize(String tnmSize) {
        this.tnmSize = tnmSize;
    }
}
