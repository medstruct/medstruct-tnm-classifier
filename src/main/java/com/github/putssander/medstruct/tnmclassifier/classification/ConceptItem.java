package com.github.putssander.medstruct.tnmclassifier.classification;

import java.util.ArrayList;
import java.util.List;

public class ConceptItem {

    private String uri;

    private String text;

    private List<Integer> tokens = new ArrayList<>();

    public ConceptItem() {
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Integer> getTokens() {
        return tokens;
    }

    public void setTokens(List<Integer> tokens) {
        this.tokens = tokens;
    }
}
