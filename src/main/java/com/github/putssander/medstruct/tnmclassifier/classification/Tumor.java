package com.github.putssander.medstruct.tnmclassifier.classification;

import com.github.putssander.medstruct.tnmclassifier.constants.BodySide;
import com.github.putssander.medstruct.tnmclassifier.constants.LungLobe;

public class Tumor {



    public Tumor(Double size, String unit, BodySide bodySide, LungLobe lungLobe, int sentenceId) {
        this.size = size;
        this.unit = unit;
        this.bodySide = bodySide;
        this.lungLobe = lungLobe;
        this.sentenceId = sentenceId;
    }

    private Double size;

    private final String unit;

    private BodySide bodySide;

    private LungLobe lungLobe;

    private int sentenceId;

    private Boolean avide = null;

    public Double getSize() {
        return size;
    }

    public void setSize(Double size) {
        this.size = size;
    }

    public String getUnit() {
        return unit;
    }

    public BodySide getBodySide() {
        return bodySide;
    }

    public void setBodySide(BodySide bodySide) {
        this.bodySide = bodySide;
    }

    public LungLobe getLungLobe() {
        return lungLobe;
    }

    public void setLungLobe(LungLobe lungLobe) {
        this.lungLobe = lungLobe;
    }

    public int getSentenceId() {
        return sentenceId;
    }

    public void setSentenceId(int sentenceId) {
        this.sentenceId = sentenceId;
    }

    public Boolean getAvide() {
        return avide;
    }

    public void setAvide(Boolean avide) {
        this.avide = avide;
    }
    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (!(obj instanceof Tumor))
            return false;
        Tumor other = (Tumor)obj;

        return size.equals(other.getSize()) && lungLobe.equals(other.getLungLobe());
    }

    @Override
    public String toString() {
        return "Tumor{" +
                "size=" + size +
                ", unit='" + unit + '\'' +
                ", bodySide=" + bodySide +
                ", lungLobe=" + lungLobe +
                ", sentenceId=" + sentenceId +
                ", avide=" + avide +
                '}';
    }
}
