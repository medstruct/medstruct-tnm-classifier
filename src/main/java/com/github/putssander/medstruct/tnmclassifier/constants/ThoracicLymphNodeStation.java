package com.github.putssander.medstruct.tnmclassifier.constants;

public enum ThoracicLymphNodeStation {
    ONE, TWO, THREE_A, THREE_P, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, ELEVEN, TWELVE, THIRTEEN, FOURTEEN
}
