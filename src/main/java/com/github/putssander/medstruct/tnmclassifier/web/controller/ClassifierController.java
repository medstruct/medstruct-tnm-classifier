package com.github.putssander.medstruct.tnmclassifier.web.controller;

import com.github.putssander.medstruct.tnmclassifier.classification.TnmClassificationRequest;
import com.github.putssander.medstruct.tnmclassifier.classification.TnmClassificationResponse;
import com.github.putssander.medstruct.tnmclassifier.service.TnmClassificationService;
import com.github.putssander.medstruct.tnmclassifier.util.JsonNlpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.*;

@RestController
@ConditionalOnProperty(prefix="restcontroller", name="enabled")
@RequestMapping("/api")
public class ClassifierController {

    private static final Logger logger = LoggerFactory.getLogger(ClassifierController.class);

    private TnmClassificationService tnmClassificationService;

    public ClassifierController(TnmClassificationService tnmClassificationService) {
        logger.info("restcontroller.enabled=true");
        this.tnmClassificationService = tnmClassificationService;
    }

    @PostMapping("/classify/tnm/lung8/jsonlp")
    public String getTnmClassification(@RequestBody String jsonNlp){
        logger.info("INPUT");
        jsonNlp = this.tnmClassificationService.classify(jsonNlp);
        logger.info("OUTPUT");
        return jsonNlp;
    }

    @PostMapping("/classify/tnm/lung8")
    public TnmClassificationResponse getTnmClassificationRules(
            @RequestBody TnmClassificationRequest tnmClassificationRequest){
        logger.info("retrieve rule classification");
        TnmClassificationResponse response = this.tnmClassificationService.classifyRules(tnmClassificationRequest);
        logger.info("OUTPUT");
        return response;
    }

    @GetMapping("/classify/tnm/lung8")
    public String getTnmClassificationRules(
            ){
        return "okidoki";
    }
}
