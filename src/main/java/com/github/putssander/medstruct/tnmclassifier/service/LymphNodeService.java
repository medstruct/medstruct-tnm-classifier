package com.github.putssander.medstruct.tnmclassifier.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.putssander.medstruct.tnmclassifier.classification.*;
import com.github.putssander.medstruct.tnmclassifier.configuration.properties.BodySideConceptProperties;
import com.github.putssander.medstruct.tnmclassifier.constants.BodySide;
import com.github.putssander.medstruct.tnmclassifier.constants.ThoracicLymphNodeClassificationMethod;
import com.github.putssander.medstruct.tnmclassifier.constants.ThoracicLymphNodeStation;
import com.github.putssander.medstruct.tnmclassifier.configuration.properties.ThoracicLymphNodeConceptProperties;
import com.github.putssander.medstruct.tnmclassifier.configuration.properties.ThoracicLymphNodeStationProperties;
import com.github.putssander.medstruct.tnmclassifier.util.JsonNlpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static com.github.putssander.medstruct.tnmclassifier.constants.BodySide.*;
import static com.github.putssander.medstruct.tnmclassifier.constants.BodySide.UNKNOWN;
import static com.github.putssander.medstruct.tnmclassifier.constants.ThoracicLymphNodeClassificationMethod.*;

@Service
@EnableConfigurationProperties({
        ThoracicLymphNodeStationProperties.class,
        ThoracicLymphNodeConceptProperties.class,
        BodySideConceptProperties.class})
public class LymphNodeService {

    private static final Logger logger = LoggerFactory.getLogger(LymphNodeService.class);

    private ThoracicLymphNodeStationProperties thoracicLymphNodeStationProperties;
    private ThoracicLymphNodeConceptProperties thoracicLymphNodeConceptProperties;
    private BodySideConceptProperties bodySideConceptProperties;
    private ObjectMapper objectMapper;
    private ContextService contextService;

    private List<ThoracicLymphNodeStation> noLymphBodySideDetectionStation = Arrays.asList(
            ThoracicLymphNodeStation.ONE,
            ThoracicLymphNodeStation.THREE_A,
            ThoracicLymphNodeStation.THREE_P,
            ThoracicLymphNodeStation.FIVE,
            ThoracicLymphNodeStation.SIX,
            ThoracicLymphNodeStation.SEVEN);

    public LymphNodeService(ThoracicLymphNodeStationProperties thoracicLymphNodeStationProperties,
                            ThoracicLymphNodeConceptProperties thoracicLymphNodeConceptProperties, BodySideConceptProperties bodySideConceptProperties, ObjectMapper objectMapper, ContextService contextService) {
        this.thoracicLymphNodeStationProperties = thoracicLymphNodeStationProperties;
        this.thoracicLymphNodeConceptProperties = thoracicLymphNodeConceptProperties;
        this.bodySideConceptProperties = bodySideConceptProperties;
        this.objectMapper = objectMapper;
        this.contextService = contextService;
    }


    public List<LymphNode> getLymphNodes(JsonNode document,
                                         List<Integer> bodySideRightTokens,
                                         List<Integer> bodySideLeftTokens, BodySide tumorBodySide,
                                         List<ConceptItem> avideConceptPresentItems,
                                         List<ConceptItem> avideConceptNotPresentItems) throws Exception {

        List<LymphNode> lymphNodes = new ArrayList<>();

        //get lymph measurements extracted by measurement extractor
        lymphNodes.addAll(getLymphMeasurements(document));

        //get lymphadenopathy context validated
        lymphNodes.addAll(getLymphAdenopathy(document));

        //get pathologic lymph nodes by context validated pathologic expression
        lymphNodes.addAll(getLymphNodesPathologic(document));

        //set for all found nodes avide
        lymphNodes = setAvideLymphNodes(document, lymphNodes, avideConceptPresentItems, avideConceptNotPresentItems);

        extendBodySideTokensWithLaterality(document, bodySideRightTokens, bodySideLeftTokens, tumorBodySide);
        detectBodySide(bodySideRightTokens, bodySideLeftTokens, document, lymphNodes);

        duplicateBilateraleralNodes(document, lymphNodes);
        return lymphNodes;
    }

    private List<LymphNode> setAvideLymphNodes(JsonNode document, List<LymphNode> lymphNodes, List<ConceptItem> avideConceptPresentItems, List<ConceptItem> avideConceptNotPresentItems) {
        try{
            List<LymphNode> avideLymphNodes = getAvideLymphNodes(document, avideConceptPresentItems);
            List<LymphNode> nonAvideLymphNodes = getNonAvideLymphNodes(document, avideConceptNotPresentItems);
            nonAvideLymphNodes.stream().forEach(node -> {
                if(lymphNodes.contains(node)) {
                    Iterator<LymphNode> itr = lymphNodes.iterator();
                    while (itr.hasNext()) {
                        LymphNode current = itr.next();
                        if(current.equals(node)) current.setAvide(false);
                    }
                }
            });
            avideLymphNodes.stream().forEach(node -> {
                if(lymphNodes.contains(node)) {
                    Iterator<LymphNode> itr = lymphNodes.iterator();
                    while (itr.hasNext()) {
                        LymphNode current = itr.next();
                        if(current.equals(node)) current.setAvide(true);
                    }
                } else {
                    lymphNodes.add(node);
                }
            });
        }
        catch (Exception e){
            logger.error("Unable to set avide lymph nodes", e);
        }
        return lymphNodes;
    }





    private void duplicateBilateraleralNodes(JsonNode document, List<LymphNode> lymphNodes){
        List<Integer> tokensBilateral = JsonNlpUtil.getTokensForUri(document, bodySideConceptProperties.getBilateral())
                .stream().map(node -> node.get("id").asInt()).collect(Collectors.toList());

        List<LymphNode> lymphNodeBilateralClones = new ArrayList<>();
        for(LymphNode node : lymphNodes) {
            if (node.getBodySideTokenId() != null && tokensBilateral.contains(node.getBodySideTokenId()))
                lymphNodeBilateralClones.add(getBilateralClone(node));
        }
        lymphNodes.addAll(lymphNodeBilateralClones);
    }

    private void extendBodySideTokensWithLaterality(JsonNode document, List<Integer> bodySideRightTokens, List<Integer> bodySideLeftTokens, BodySide tumorBodySide) {
        List<Integer> tokensBilateral = JsonNlpUtil.getTokensForUri(document, bodySideConceptProperties.getBilateral())
                .stream().map(node -> node.get("id").asInt()).collect(Collectors.toList());
        List<Integer> tokensContralateral = JsonNlpUtil.getTokensForUri(document, bodySideConceptProperties.getContralateral())
                .stream().map(node -> node.get("id").asInt()).collect(Collectors.toList());
        List<Integer> tokensIpsilateral = JsonNlpUtil.getTokensForUri(document, bodySideConceptProperties.getIpsilateral())
                .stream().map(node -> node.get("id").asInt()).collect(Collectors.toList());

        switch (tumorBodySide) {
            case LEFT:
                bodySideRightTokens.addAll(tokensContralateral);
                bodySideRightTokens.addAll(tokensBilateral);
                bodySideLeftTokens.addAll(tokensIpsilateral);
                break;
            case RIGHT:
                bodySideLeftTokens.addAll(tokensContralateral);
                bodySideLeftTokens.addAll(tokensBilateral);
                bodySideRightTokens.addAll(tokensIpsilateral);
                break;
            case UNKNOWN:
                logger.info("Tumorside unknown cannot deduce laterality");
        }

    }


    public List<LymphNode> getLymphMeasurements(JsonNode document) {

        List<LymphNode> lymphNodes = new ArrayList<>();
        try {
            lymphNodes.addAll(getMeasuredLymphNodes(document));
        }
        catch (Exception e){
            logger.error("Unable to set measured lymph nodes", e);
        }
        return lymphNodes;
    }


    public List<LymphNode> getMeasuredLymphNodes(JsonNode document) throws IOException {
        List<LymphNode> lymphNodes = new ArrayList<>();
        String uri = thoracicLymphNodeConceptProperties.getLymphNodeSize();
        List<MeasurementExpression> measurements = JsonNlpUtil.getMeasurements(getMeasurements(document), uri);
        measurements.forEach(measurement -> {
            double value = measurement.getNormalizedValues().get(0).doubleValue();
            String unit = measurement.getNormalizedUnit();
            double minPathologicLymphNodeSize = 1;
            if(value > minPathologicLymphNodeSize) {
                try {
                    LymphNodeStationMention station = detectMeasurementStation(document, measurement.getTokens().get(0));
                    if(station != null)
                        lymphNodes.add(new LymphNode(
                                SIZE,
                                station.getThoracicLymphNodeStation(),
                                getConcept(station.getThoracicLymphNodeStation()),
                                measurement.getCodeTokens(),
                                measurement.getTokens(),
                                value,
                                station.getTokenId()));
                } catch (Exception e) {
                    logger.error("Error extracting lymph station for tokenId = {}", measurement.getCodeTokens().get(0));
                }
            }
        });
        return lymphNodes;
    }



    private List<LymphNode> getLymphAdenopathy(JsonNode document){
        List<LymphNode> lymphNodes = new ArrayList<>();
        for(ConceptItem item : contextService.getConceptPresentItems(document, thoracicLymphNodeConceptProperties.getLymphAdenopathy())){
            try {
                List<LymphNodeStationMention> stations = detectStations(document, item.getTokens().get(0));
                stations.forEach(station -> lymphNodes.add(new LymphNode(
                        PATHOLOGIC,
                        station.getThoracicLymphNodeStation(),
                        getConcept(station.getThoracicLymphNodeStation()),
                        item.getTokens(),
                        new ArrayList<>(),
                        station.getTokenId())));
            } catch (Exception e) {
                logger.error("Error detecting lymph node station for item = {}", item);
            }
        }
        return lymphNodes;
    }


    private List<LymphNode> getLymphNodesPathologic(JsonNode document) {
        List<LymphNode> lymphNodes = new ArrayList<>();
        try {
            lymphNodes.addAll(getPathologicLymphNodes(document));
        }
        catch (Exception e){
            logger.error("Unable to set pathologic lymph nodes", e);
        }
        return lymphNodes;
    }


    public List<LymphNode> getPathologicLymphNodes(JsonNode document) {
        List<ConceptItem> adjContextItems = contextService.getConceptPresentItems(document, thoracicLymphNodeConceptProperties.getLymphPathologicAdj());
        List<LymphNode> nodes = getSameSentenceLymphNodes(document, adjContextItems, PATHOLOGIC);
        return nodes;
    }
    
    public List<LymphNode> getAvideLymphNodes(JsonNode document, List<ConceptItem> avideConcepItems) {
        List<LymphNode> nodes = getSameSentenceLymphNodes(document, avideConcepItems, AVIDE);
        nodes.stream().forEach(node -> node.setAvide(true));
        return nodes;
    }

    public List<LymphNode> getNonAvideLymphNodes(JsonNode document, List<ConceptItem> avideConcepItems) {
        List<LymphNode> nodes =  getSameSentenceLymphNodes(document, avideConcepItems, AVIDE);
        nodes.stream().forEach(node -> node.setAvide(false));
        return nodes;
    }

    public List<LymphNode> getSameSentenceLymphNodes(JsonNode document,
                                                     List<ConceptItem> conceptItems,
                                                     ThoracicLymphNodeClassificationMethod method) {
        List<Integer> lymphTokens = JsonNlpUtil.getTokensForUri(document, thoracicLymphNodeConceptProperties.getLymph())
                .stream()
                .map(token -> JsonNlpUtil.getTokenId(token))
                .collect(Collectors.toList());

        List<Integer> conceptStartTokenList = conceptItems
                .stream()
                .map(conceptItem -> conceptItem.getTokens().get(0))
                .collect(Collectors.toList());

        List<LymphNode> lymphNodes = new ArrayList<>();
        for(Integer lymphToken : lymphTokens){
            for(Integer conceptStartToken : conceptStartTokenList) {
                if(JsonNlpUtil.isInSameSentence(document, lymphToken, conceptStartToken)) {
                    try {
                        List<LymphNodeStationMention> stations = detectStations(document, lymphToken);
                        stations.forEach(station -> {
                            LymphNode lymphNode = new LymphNode(
                                    method,
                                    station.getThoracicLymphNodeStation(),
                                    getConcept(station.getThoracicLymphNodeStation()),
                                    Arrays.asList(lymphToken),
                                    Arrays.asList(conceptStartToken), station.getTokenId());
                            lymphNodes.add(lymphNode);
                        });
                        break;
                    } catch (Exception e) {
                        logger.error("Error detecting lymph node station for lymphToken = {}", lymphNodes);
                    }
                }
            }
        }
        return lymphNodes;
    }

    private List<MeasurementExpression> getMeasurements(JsonNode document) throws IOException {
        return JsonNlpUtil.getMeasurements(objectMapper, document);
    }

    private String getConcept(ThoracicLymphNodeStation thoracicLymphNodeStation) {
        switch(thoracicLymphNodeStation) {
            case ONE: return thoracicLymphNodeStationProperties.getOne();
            case TWO: return thoracicLymphNodeStationProperties.getTwo();
            case THREE_A: return thoracicLymphNodeStationProperties.getThreeA();
            case THREE_P: return thoracicLymphNodeStationProperties.getThreeP();
            case FOUR: return thoracicLymphNodeStationProperties.getFour();
            case FIVE: return thoracicLymphNodeStationProperties.getFive();
            case SIX: return thoracicLymphNodeStationProperties.getSix();
            case SEVEN: return thoracicLymphNodeStationProperties.getSeven();
            case EIGHT: return thoracicLymphNodeStationProperties.getEight();
            case NINE: return thoracicLymphNodeStationProperties.getNine();
            case TEN: return thoracicLymphNodeStationProperties.getTen();
            case ELEVEN: return thoracicLymphNodeStationProperties.getEleven();
            case TWELVE: return thoracicLymphNodeStationProperties.getTwelve();
            case THIRTEEN: return thoracicLymphNodeStationProperties.getThirteen();
            case FOURTEEN: return thoracicLymphNodeStationProperties.getFourteen();
            default:
                logger.warn("region not found");
                return null;

        }
    }

    private List<LymphNodeStationMention> detectStations(JsonNode document, int lymphNodeToken) throws Exception {

        List<LymphNodeStationMention> lymphNodeStationList = new ArrayList<>();
        for(ThoracicLymphNodeStation station : ThoracicLymphNodeStation.values()){
            List<Integer> stationTokens = JsonNlpUtil.getTokensIdForUri(document, getConcept(station));
            if(stationTokens.isEmpty())
                continue;
            for(Integer stationToken : stationTokens) {
                if (JsonNlpUtil.isInSameSentence(document, lymphNodeToken, stationToken)) {
                    lymphNodeStationList.add(new LymphNodeStationMention(station, stationToken));
                }
            }
        }
        return lymphNodeStationList;
    }

    private LymphNodeStationMention detectMeasurementStation(JsonNode document, Integer measurementTokenId) {

        Map<Integer, ThoracicLymphNodeStation> stationMap = getStationTokenIdMap(document);

        int sentenceId = JsonNlpUtil.getSentenceId(document, measurementTokenId);
        int punctuationFrom = JsonNlpUtil.getFirstPunctuationIdBackwards(document, measurementTokenId);
        int punctuationTo = JsonNlpUtil.getFirstPunctuationIdForward(document, measurementTokenId);
        int sentenceFrom = JsonNlpUtil.getSentenceTokenFrom(document, sentenceId);
        int sentenceTo = JsonNlpUtil.getSentenceTokenTo(document, sentenceId);

        // backwards to punctuation
        for(int i = measurementTokenId; i > punctuationFrom; i--){
            if(stationMap.containsKey(i))
                return new LymphNodeStationMention(stationMap.get(i), i);
        }
        // forwards to punctuation
        for(int i = measurementTokenId; i < punctuationTo; i++){
            if(stationMap.containsKey(i))
                return new LymphNodeStationMention(stationMap.get(i), i);
        }
        // backwards to sentence
        for(int i = punctuationFrom; i >= sentenceFrom; i--){
            if(stationMap.containsKey(i))
                return new LymphNodeStationMention(stationMap.get(i), i);
        }
        // forwards to sentence
        for(int i = punctuationTo; i < sentenceTo; i++){
            if(stationMap.containsKey(i))
                return new LymphNodeStationMention(stationMap.get(i), i);
        }

        logger.warn("No lymph station found for lymph measurement tokenId={}", measurementTokenId);
        return null;
    }

    private Map<Integer, ThoracicLymphNodeStation> getStationTokenIdMap(JsonNode document){
        Map<Integer, ThoracicLymphNodeStation> lymphNodeMap = new HashMap<>();
        for(ThoracicLymphNodeStation station : ThoracicLymphNodeStation.values()){
            List<Integer> stationTokenIds = JsonNlpUtil.getTokensIdForUri(document, getConcept(station));
            if(stationTokenIds.isEmpty())
                continue;
            for(Integer stationTokenId : stationTokenIds) {
                lymphNodeMap.put(stationTokenId, station);
            }
        }
        return lymphNodeMap;
    }

    private void detectBodySide(List<Integer> bodySideRightTokens, List<Integer> bodySideLeftTokens, JsonNode document, List<LymphNode> lymphNodes) {
        lymphNodes.forEach(lymphNode -> {
            BodySideMention bodySideMention = getBodySideMention(bodySideRightTokens, bodySideLeftTokens, document, lymphNode);
            if(bodySideMention == null)
                lymphNode.setBodySide(UNKNOWN);
            else {
                lymphNode.setBodySide(bodySideMention.getBodySide());
                lymphNode.setBodySideMention(bodySideMention);
            }
        });
    }

    private BodySideMention getBodySideMention(List<Integer> bodySideRightTokens, List<Integer> bodySideLeftTokens, JsonNode document, LymphNode lymphNode) {
        if(lymphNode.getThoracicLymphNodeStation().equals(ThoracicLymphNodeStation.FIVE) ||
                lymphNode.getThoracicLymphNodeStation().equals(ThoracicLymphNodeStation.SIX)){
            lymphNode.setBodySide(BodySide.LEFT);
        }
        if(!isDetectBodySide(lymphNode.getThoracicLymphNodeStation()))
            return null;
        else
            return BodySideService.getTokenBodySideMention(bodySideRightTokens, bodySideLeftTokens, document, Arrays.asList(lymphNode.getStationTokenId()));
    }

    private boolean isDetectBodySide(ThoracicLymphNodeStation thoracicLymphNodeStation){
        if(noLymphBodySideDetectionStation.contains(thoracicLymphNodeStation))
            return false;
        return true;
    }

    public static LymphNode getBilateralClone(LymphNode lymphNode){
        LymphNode copy = new LymphNode();
        copy.setThoracicLymphNodePathologicScheme(lymphNode.getThoracicLymphNodePathologicScheme());
        copy.setThoracicLymphNodeClassificationMethod(lymphNode.getThoracicLymphNodeClassificationMethod());
        copy.setThoracicLymphNodeStation(lymphNode.getThoracicLymphNodeStation());
        copy.setSizeCm(lymphNode.getSizeCm());
        copy.setLymphTokens(lymphNode.getLymphTokens());
        copy.setLymphPropertyTokens(lymphNode.getLymphPropertyTokens());
        copy.setStationTokenId(lymphNode.getStationTokenId());
        copy.setBodySideTokenId(lymphNode.getBodySideTokenId());
        copy.setUri(lymphNode.getUri());

        if(lymphNode.getBodySide() == BodySide.RIGHT)
            copy.setBodySide(BodySide.LEFT);
        else
            copy.setBodySide(RIGHT);
        return copy;
    }

}


