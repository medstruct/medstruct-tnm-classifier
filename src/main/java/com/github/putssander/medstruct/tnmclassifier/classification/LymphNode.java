package com.github.putssander.medstruct.tnmclassifier.classification;

import com.github.putssander.medstruct.tnmclassifier.constants.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

public class LymphNode {

    private ThoracicLymphNodePathologicScheme thoracicLymphNodePathologicScheme = ThoracicLymphNodePathologicScheme.UNKNOWN;

    private ThoracicLymphNodeClassificationMethod thoracicLymphNodeClassificationMethod = ThoracicLymphNodeClassificationMethod.UNKNOWN;

    private ThoracicLymphNodeStation thoracicLymphNodeStation;

    private Double sizeCm;

    private BodySide bodySide = BodySide.UNKNOWN;

    private List<Integer> lymphTokens = new ArrayList<>();

    private List<Integer> lymphPropertyTokens = new ArrayList<>();

    private Integer stationTokenId;

    private Integer bodySideTokenId;

    private String uri;

    private Boolean avide = null;

    public LymphNode(){}

    //constructor for measurement
    public LymphNode(ThoracicLymphNodeClassificationMethod classificationMethod,
                     ThoracicLymphNodeStation thoracicLymphNodeStation,
                     String uri,
                     List<Integer> lymphTokens, List<Integer> lymphPropertyTokens,  Double sizeCm, int stationTokenId) {
        this.thoracicLymphNodeClassificationMethod = classificationMethod;
        this.thoracicLymphNodeStation = thoracicLymphNodeStation;
        this.uri = uri;
        this.lymphTokens = lymphTokens;
        this.lymphPropertyTokens = lymphPropertyTokens;
        this.sizeCm = sizeCm;
        this.stationTokenId = stationTokenId;
    }

    //constructor for adenopathy or pathologic adj
    public LymphNode(ThoracicLymphNodeClassificationMethod classificationMethod,
                     ThoracicLymphNodeStation thoracicLymphNodeStation,
                     String uri,
                     List<Integer> lymphTokens, List<Integer> lymphPropertyTokens, int stationTokenId) {
        this.thoracicLymphNodeClassificationMethod = classificationMethod;
        this.thoracicLymphNodeStation = thoracicLymphNodeStation;
        this.uri = uri;
        this.lymphTokens = lymphTokens;
        this.lymphPropertyTokens = lymphPropertyTokens;
        this.stationTokenId = stationTokenId;
    }

    //constructor for gui
    public LymphNode(ThoracicLymphNodeStation lymphNodeStation,
                     BodySide bodySide,
                     ThoracicLymphNodeClassificationMethod classificationMethod) {
        this.thoracicLymphNodeClassificationMethod = classificationMethod;
        this.thoracicLymphNodeStation = lymphNodeStation;
        this.bodySide = bodySide;
    }

    public ThoracicLymphNodeStation getThoracicLymphNodeStation() {
        return thoracicLymphNodeStation;
    }

    public void setThoracicLymphNodeStation(ThoracicLymphNodeStation thoracicLymphNodeStation) {
        this.thoracicLymphNodeStation = thoracicLymphNodeStation;
    }

    public ThoracicLymphNodePathologicScheme getThoracicLymphNodePathologicScheme() {
        return thoracicLymphNodePathologicScheme;
    }

    public void setThoracicLymphNodePathologicScheme(ThoracicLymphNodePathologicScheme thoracicLymphNodePathologicScheme) {
        this.thoracicLymphNodePathologicScheme = thoracicLymphNodePathologicScheme;
    }

    public ThoracicLymphNodeClassificationMethod getThoracicLymphNodeClassificationMethod() {
        return thoracicLymphNodeClassificationMethod;
    }

    public void setThoracicLymphNodeClassificationMethod(ThoracicLymphNodeClassificationMethod thoracicLymphNodeClassificationMethod) {
        this.thoracicLymphNodeClassificationMethod = thoracicLymphNodeClassificationMethod;
    }

    public Double getSizeCm() {
        return sizeCm;
    }

    public void setSizeCm(Double sizeCm) {
        this.sizeCm = sizeCm;
    }

    public BodySide getBodySide() {
        return bodySide;
    }

    public void setBodySide(BodySide bodySide) {
        this.bodySide = bodySide;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public List<Integer> getLymphTokens() {
        return lymphTokens;
    }

    public void setLymphTokens(List<Integer> lymphTokens) {
        this.lymphTokens = lymphTokens;
    }

    public List<Integer> getLymphPropertyTokens() {
        return lymphPropertyTokens;
    }

    public void setLymphPropertyTokens(List<Integer> lymphPropertyTokens) {
        this.lymphPropertyTokens = lymphPropertyTokens;
    }

    public Integer getStationTokenId() {
        return stationTokenId;
    }

    public void setStationTokenId(Integer stationTokenId) {
        this.stationTokenId = stationTokenId;
    }

    public Integer getBodySideTokenId() {
        return bodySideTokenId;
    }

    public void setBodySideTokenId(Integer bodySideTokenId) {
        this.bodySideTokenId = bodySideTokenId;
    }

    @Override
    public String toString() {
        return "LymphNode{" +
                "thoracicLymphNodePathologicScheme=" + thoracicLymphNodePathologicScheme +
                ", thoracicLymphNodeClassificationMethod=" + thoracicLymphNodeClassificationMethod +
                ", thoracicLymphNodeStation=" + thoracicLymphNodeStation +
                ", sizeCm=" + sizeCm +
                ", bodySide=" + bodySide +
                ", lymphTokens=" + lymphTokens +
                ", lymphPropertyTokens=" + lymphPropertyTokens +
                ", uri='" + uri + '\'' +
                ", avide='" + avide + '\'' +
                '}';
    }

    public void setBodySideMention(BodySideMention bodySideMention) {
        this.bodySide = bodySideMention.getBodySide();
        this.bodySideTokenId = bodySideMention.getFirstTokenId();
    }

    public Boolean isAvide() {
        return avide;
    }

    public void setAvide(Boolean avide) {
        this.avide = avide;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LymphNode lymphNode = (LymphNode) o;
        HashSet<Integer> s1 = new HashSet<>(lymphTokens);
        HashSet<Integer> s2 = new HashSet<>(lymphNode.lymphTokens);
        s1.retainAll(s2);
        boolean tokensMatch = s1.size() > 0;
        boolean bodySideMatch =
                this.isBodySideUnknown()
                || lymphNode.isBodySideUnknown()
                || this.getBodySide().equals(lymphNode.getBodySide());
        boolean stationMatch =
                this.isStationUnknown()
                || lymphNode.isStationUnknown()
                || this.getThoracicLymphNodeStation().equals(lymphNode.getThoracicLymphNodeStation());
        return tokensMatch && bodySideMatch && stationMatch;
    }

    @Override
    public int hashCode() {
        return Objects.hash(lymphTokens);
    }

    public boolean isBodySideUnknown(){
        return this.getBodySide().equals(null) || this.getBodySide().equals(BodySide.NONE) || this.getBodySide().equals(BodySide.UNKNOWN);
    }

    public boolean isStationUnknown(){
        return this.getThoracicLymphNodeStation().equals(null);
    }
}
