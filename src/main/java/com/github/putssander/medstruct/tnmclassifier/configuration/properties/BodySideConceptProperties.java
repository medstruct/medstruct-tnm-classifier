package com.github.putssander.medstruct.tnmclassifier.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("bodyside-concepts")
public class BodySideConceptProperties {

    private String ipsilateral = "none:side-ipsilateral";

    private String contralateral = "none:side-contralateral";

    private String bilateral = "none:side-bilateral";

    public String getIpsilateral() {
        return ipsilateral;
    }

    public void setIpsilateral(String ipsilateral) {
        this.ipsilateral = ipsilateral;
    }

    public String getContralateral() {
        return contralateral;
    }

    public void setContralateral(String contralateral) {
        this.contralateral = contralateral;
    }

    public String getBilateral() {
        return bilateral;
    }

    public void setBilateral(String bilateral) {
        this.bilateral = bilateral;
    }
}
