package com.github.putssander.medstruct.tnmclassifier.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.putssander.medstruct.tnmclassifier.classification.*;
import com.github.putssander.medstruct.tnmclassifier.constants.BodySide;
import com.github.putssander.medstruct.tnmclassifier.configuration.properties.TnmProperties;
import com.github.putssander.medstruct.tnmclassifier.util.JsonNlpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.github.putssander.medstruct.tnmclassifier.constants.MedStructConstants.*;
import static com.github.putssander.medstruct.tnmclassifier.constants.MedStructConstants.LAYER_CONTEXT;

@Service
@EnableConfigurationProperties(TnmProperties.class)
public class TnmClassificationService {

    private static final Logger logger = LoggerFactory.getLogger(TnmClassificationService.class);

    private final Pattern lungSizeBlacklistPattern;

    private TnmProperties tnmProperties;
    private ContextService contextService;
    private ObjectMapper objectMapper;

    private LymphNodeService lymphNodeService;
    private LymphNodeClassificationService lymphNodeClassificationService;
    private BodySideService bodySideService;

    public TnmClassificationService(TnmProperties tnmProperties,
                                    ContextService contextService,
                                    ObjectMapper objectMapper,
                                    LymphNodeService lymphNodeService,
                                    LymphNodeClassificationService lymphNodeClassificationService, BodySideService bodySideService) {
        this.tnmProperties = tnmProperties;
        this.contextService = contextService;
        this.objectMapper = objectMapper;
        this.lungSizeBlacklistPattern = Pattern.compile(tnmProperties.getLungSizeBlacklistPattern(), Pattern.CASE_INSENSITIVE);
        this.lymphNodeService = lymphNodeService;
        this.lymphNodeClassificationService = lymphNodeClassificationService;
        this.bodySideService = bodySideService;
    }

    public String classify(String jsonNlp){
        try {
            JsonNode jsonNlpNode = JsonNlpUtil.getJsonNode(jsonNlp, objectMapper);
            JsonNode document = JsonNlpUtil.getDocument(jsonNlpNode);
            logger.info("Parsed JSONNLP document with id={}", JsonNlpUtil.getDocumentId(document));
            TnmClassification tnmClassification = classify(document);
            JsonNode result = JsonNlpUtil.createJsonNode(tnmClassification, objectMapper);
            document = JsonNlpUtil.addModuleResult(document, result, "tnm-classification");
            jsonNlp = JsonNlpUtil.refreshDocument(jsonNlpNode, document, objectMapper);
        }
        catch(Exception e){
            logger.error("Exception", e);
        }
        return jsonNlp;
    }


    public TnmClassification classify(JsonNode document){
        TnmClassification tnmClassification = new TnmClassification();
        if(!validateLayers(document)){
            return tnmClassification;
        }
        List<ConceptItem> involvementConceptItems = getInvolvementPresentItems(document);

        List<ConceptItem> avideConceptPresentItems = getAvidePresentItems(document);
        List<ConceptItem> avideConceptNotPresentItems = getAvideNotPresentItems(document);

        List<MeasurementExpression> filteredMeasurementExpressions = new ArrayList<>();

        List<Integer> bodySideRightTokens = bodySideService.getBodySideRightTokenIds(document);
        List<Integer> bodySideLeftTokens = bodySideService.getBodySideLeftTokenIds(document);
        List<Integer> primaryTumorMeasurementTokens = new ArrayList<>();
        try {
            List<MeasurementExpression> measurementExpressions = getMeasurements(document);

            filteredMeasurementExpressions.addAll(getTumorMeasurements(measurementExpressions));
            filteredMeasurementExpressions.addAll(MedStructService.getMeasurementDimensionWithoutCode(measurementExpressions));
            filteredMeasurementExpressions.addAll(MedStructService.getMeasurementLengthWithoutCodeAndSubtype(measurementExpressions));
            if(tnmProperties.isApplyLungSizeBlacklistPattern()){
                filteredMeasurementExpressions = filteredMeasurementExpressions
                        .stream().filter(measurementExpression ->
                                !isBlacklistSentence(document, measurementExpression, lungSizeBlacklistPattern))
                        .collect(Collectors.toList());
            }

            Optional<BigDecimal> tumorSize = MedStructService.getMaxMeasurement(filteredMeasurementExpressions, tnmProperties.getMaxValidTumorSizeCm());

            if (tumorSize.isPresent()) {
                tnmClassification.setTumorSize(tumorSize.get().doubleValue());
                primaryTumorMeasurementTokens = getTumorMeasurementTokens(filteredMeasurementExpressions, tumorSize.get());
            }
        }
        catch(Exception e){
            logger.warn("Error parsing measurements", e);
        }
        tnmClassification.setTnmSize(getTnmSize(tnmClassification.getTumorSize()));

        try {
            setTnm1(tnmClassification, document, involvementConceptItems);
            setTnm2(tnmClassification, document, involvementConceptItems);
            setTnm3(tnmClassification, document, involvementConceptItems);
            setTnm4(tnmClassification, document, involvementConceptItems, filteredMeasurementExpressions);

            tnmClassification.setTnm(getTnm(tnmClassification));

            BodySide tumorBodySide = getTumorBodySide(document, tnmClassification, bodySideRightTokens, bodySideLeftTokens, primaryTumorMeasurementTokens);

            List<LymphNode> lymphNodes = lymphNodeService.getLymphNodes(document, bodySideRightTokens, bodySideLeftTokens, tumorBodySide,
                    avideConceptPresentItems, avideConceptNotPresentItems);
            String n = lymphNodeClassificationService.classifyN(tumorBodySide, lymphNodes);

            tnmClassification.setLymphNodes(removeNonAvideLymphNodes(lymphNodes));
            tnmClassification.setTnmN(n);
            tnmClassification.setTnm(tnmClassification.getTnm() + n);

        }catch (Exception e){
            logger.error("Error in tnm rules", e);
        }
        return tnmClassification;
    }

    private List<LymphNode> removeNonAvideLymphNodes(List<LymphNode> lymphNodes) {
        return lymphNodes.stream().filter(lymphNode -> lymphNode.isAvide() == null || lymphNode.isAvide() == true).collect(Collectors.toList());
    }


    private BodySide getTumorBodySide(JsonNode document, TnmClassification tnmClassification, List<Integer> bodySideRightTokens, List<Integer> bodySideLeftTokens, List<Integer> primaryTumorTokens) {

        if(primaryTumorTokens.isEmpty()) {
            List<JsonNode> jsonTokens = JsonNlpUtil.getTokensForUri(document, tnmProperties.getConceptTumor());
            primaryTumorTokens.addAll(jsonTokens.stream().map(jsonToken -> jsonToken.get("id").asInt()).collect(Collectors.toList()));
        }

        logger.debug("Get tumorBodySide");
        BodySideMention tumorBodySideMention = bodySideService.getTokenBodySideMention(bodySideRightTokens, bodySideLeftTokens, document, primaryTumorTokens);
        BodySide tumorBodySide = BodySide.UNKNOWN;
        if(tumorBodySideMention != null)
            tumorBodySide = tumorBodySideMention.getBodySide();
        logger.debug("Extracted tumorBodySide {}", tumorBodySide);

        tnmClassification.setTumorSide(tumorBodySide);
        return tumorBodySide;
    }


    private List<Integer> getTumorMeasurementTokens(List<MeasurementExpression> filteredMeasurementExpressions, BigDecimal bigDecimal) {
        List<MeasurementExpression> maxExpressions = filteredMeasurementExpressions
                .stream()
                .filter(measurementExpression -> containsMeasurement(measurementExpression, bigDecimal))
                .collect(Collectors.toList());

        List<Integer> tokens = maxExpressions.stream().map(maxExpression -> maxExpression.getTokens().get(0)).collect(Collectors.toList());
        return tokens;
    }

    private boolean containsMeasurement(MeasurementExpression measurementExpression, BigDecimal bigDecimal){
        Optional<BigDecimal> max = measurementExpression.getNormalizedValues().stream().max(Comparator.naturalOrder());
        return max.isPresent() && max.get().equals(bigDecimal);
    }

    private boolean isBlacklistSentence(JsonNode document, MeasurementExpression measurementExpression, Pattern lungSizeBlacklistPattern) {
        Integer sentenceId = JsonNlpUtil.getSentenceId(document, measurementExpression.getTokens().get(0));
        String sentence = JsonNlpUtil.getSentenceString(document, sentenceId);
        return lungSizeBlacklistPattern.matcher(sentence).find();
    }

    private List<ConceptItem> getInvolvementPresentItems(JsonNode document) {
        return contextService.getConceptPresentItems(document, tnmProperties.getConceptInvolvement());
    }

    private List<ConceptItem> getAvidePresentItems(JsonNode document) {
        return contextService.isSubstantialAvidity(document, tnmProperties.getAvide());
    }

    private List<ConceptItem> getAvideNotPresentItems(JsonNode document) {
        List<ConceptItem> list = new ArrayList<>();
        list.addAll(contextService.isLimitedAmount(document, tnmProperties.getAvide()));
        list.addAll(contextService.isLimitedAvidity(document, tnmProperties.getAvide()));
        list.addAll(contextService.getConceptNotPresentItems(document, tnmProperties.getAvide()));
        return list;
    }


    private String getTnm(TnmClassificationResponse tnmClassification) {
        List<String> foundLabels = new ArrayList<>();
        if(tnmClassification.getTnmSize() != null) {
            foundLabels.add(tnmClassification.getTnmSize());
        }

        if(tnmClassification.isTnmT1()) {
            foundLabels.add("T1");
        }
        if(tnmClassification.isTnmT2()) {
            foundLabels.add("T2");
        }
        if(tnmClassification.isTnmT3()) {
            foundLabels.add("T3");
        }
        if(tnmClassification.isTnmT4()) {
            foundLabels.add("T4");
        }

        if(foundLabels.isEmpty())
            return "T0";

        Collections.sort(foundLabels);
        String labelT = foundLabels.get(foundLabels.size() - 1);
        tnmClassification.setTnmT(labelT);
        return labelT;
    }

    private boolean validateLayers(JsonNode document){
        if(!document.has(LAYER_CONTEXT)){
            logger.warn("Document does not contain required layers, layer={} is missing", LAYER_CONTEXT);
            return false;
        }
        if(!document.has(LAYER_MEASUREMENTS)){
            logger.warn("Document does not contain required layers, layer={} is missing", LAYER_MEASUREMENTS);
            return false;
        }
        return true;
    }



    private TnmClassification setTnm1(TnmClassification tnmClassification, JsonNode document, List<ConceptItem> involvementConcepts) {
        List<ConceptItem> conceptsPresent = getConceptsPresent(document, tnmProperties.getConceptsPresentT1());
        List<ConceptItem> conceptsInvolved = conceptInvolved(document, involvementConcepts, tnmProperties.getConceptsInvolvementT1());
        tnmClassification.getPresentConcepts().addAll(conceptsPresent);
        tnmClassification.getInvolvedConcepts().addAll(conceptsInvolved);

        boolean t1 = !conceptsPresent.isEmpty() || !conceptsInvolved.isEmpty();
        tnmClassification.setTnmT1(t1);
        return tnmClassification;
    }

    private TnmClassification setTnm2(TnmClassification tnmClassification, JsonNode document, List<ConceptItem> involvementConcepts) {
        List<ConceptItem> conceptsPresent = getConceptsPresent(document, tnmProperties.getConceptsPresentT2());
        List<ConceptItem> conceptsInvolved = conceptInvolved(document, involvementConcepts, tnmProperties.getConceptsInvolvementT2());
        tnmClassification.getPresentConcepts().addAll(conceptsPresent);
        tnmClassification.getInvolvedConcepts().addAll(conceptsInvolved);

        boolean t2 = !conceptsPresent.isEmpty() || !conceptsInvolved.isEmpty();
        tnmClassification.setTnmT2(t2);
        return tnmClassification;
    }

    private TnmClassification setTnm3(TnmClassification tnmClassification, JsonNode document, List<ConceptItem> involvementConcepts) {
        List<ConceptItem> conceptsPresent = getConceptsPresent(document, tnmProperties.getConceptsPresentT3());
        List<ConceptItem> conceptsInvolved = conceptInvolved(document, involvementConcepts, tnmProperties.getConceptsInvolvementT3());
        List<ConceptItem> satelliteNodules = isSatelliteInSameLobe(document);
        tnmClassification.getPresentConcepts().addAll(conceptsPresent);
        tnmClassification.getInvolvedConcepts().addAll(conceptsInvolved);

        tnmClassification.setSatelliteNodules(!satelliteNodules.isEmpty());
        boolean t3 = !conceptsPresent.isEmpty() || !conceptsInvolved.isEmpty() || tnmClassification.isSatelliteNodules();
        tnmClassification.setTnmT3(t3);
        return tnmClassification;
    }

    private TnmClassification setTnm4(TnmClassification tnmClassification,
                                      JsonNode document,
                                      List<ConceptItem> involvementConcepts,
                                      List<MeasurementExpression> measurementExpressions ) {

        List<ConceptItem> conceptsPresent = getConceptsPresent(document, tnmProperties.getConceptsPresentT4());
        List<ConceptItem> conceptsInvolved = conceptInvolved(document, involvementConcepts, tnmProperties.getConceptsInvolvementT4());
        tnmClassification.getPresentConcepts().addAll(conceptsPresent);
        tnmClassification.getInvolvedConcepts().addAll(conceptsInvolved);

        tnmClassification.setTumors(MultipleTumorIpsilateralLobeService.getTumors(tnmProperties, document, measurementExpressions));
        tnmClassification.setTumorsInDiffentLobesIpsilateral(
                MultipleTumorIpsilateralLobeService.isIpsilateralTumors(document, tnmProperties, tnmClassification.getTumors(), tnmProperties.getMinTumorSize()));

        boolean t4 = !conceptsPresent.isEmpty() || !conceptsInvolved.isEmpty() || tnmClassification.isTumorsInDiffentLobesIpsilateral();
        tnmClassification.setTnmT4(t4);
        return tnmClassification;
    }

    private List<ConceptItem> getConceptsPresent(JsonNode document, List<String> concepts){
        List<ConceptItem> conceptsPresent = new ArrayList<>();
        for(String concept : concepts) {
            conceptsPresent.addAll(contextService.getConceptPresentItems(document, concept));
        }
        return conceptsPresent;
    }


    private List<ConceptItem> isSatelliteInSameLobe(JsonNode document){
        List<ConceptItem> satelliteItems = contextService.getConceptPresentItems(document, tnmProperties.getConceptSatellite());
        return satelliteItems;
    }

    private List<ConceptItem> conceptInvolved(JsonNode document, List<ConceptItem> involvementConcepts, List<String> uriList) {
        List<ConceptItem> conceptsInvolved = new ArrayList<>();
        for(String concept : uriList) {
            conceptsInvolved.addAll(getInvolvedTokens(document, concept, involvementConcepts));
        }
        return conceptsInvolved;
    }

    private String getTnmSize(Double tumorSize) {
        String tnmSize = "T0";
        if(tumorSize == null)
            return tnmSize;

        for(Map.Entry<String, Double> entry : tnmProperties.getSizeLabelLargerThan().entrySet()){
            if(tumorSize > entry.getValue() && tumorSize < tnmProperties.getMaxValidTumorSizeCm())
                tnmSize = entry.getKey();
            else break;
        }
        return tnmSize;
    }


    public List<ConceptItem> getInvolvedTokens(JsonNode document, String uri, List<ConceptItem> involvementConcepts) {
        List<ConceptItem> involvedConcepts = new ArrayList<>();
        List<ConceptItem> uriConcepts = JsonNlpUtil.getConceptsForUri(document, uri);
        for(ConceptItem conceptItem : uriConcepts){
            for(ConceptItem involvementConcept : involvementConcepts) {
                if (JsonNlpUtil.isTokensInSameSentence(
                        document,
                        conceptItem.getTokens().get(0),
                        involvementConcept.getTokens().get(0)))
                    involvedConcepts.add(conceptItem);
            }
        }
        return involvedConcepts;
    }

    private List<MeasurementExpression> getTumorMeasurements(List<MeasurementExpression> measurementExpressions) {
        return JsonNlpUtil.getMeasurements(measurementExpressions, tnmProperties.getConceptTumorSize());
    }

    private List<MeasurementExpression> getMeasurements(JsonNode document) throws IOException {
        return JsonNlpUtil.getMeasurements(objectMapper, document);
    }


    public TnmClassificationResponse classifyRules(TnmClassificationRequest tnmClassificationRequest) {
        TnmClassificationResponse tnmClassificationResponse = new TnmClassificationResponse();
        tnmClassificationResponse.setTnmSize(getTnmSize(tnmClassificationRequest.getTumorSize()));

        List<String> conceptsPresent = tnmClassificationRequest.getPresentConcepts();
        List<String> involvedConcepts = tnmClassificationRequest.getInvolvedConcepts();
        List<String> lymphNodeConcepts = tnmClassificationRequest.getLymphNodes();

        tnmClassificationResponse.setTnmT1(getTnm1(conceptsPresent, involvedConcepts));
        tnmClassificationResponse.setTnmT2(getTnm2(conceptsPresent, involvedConcepts));
        tnmClassificationResponse.setTnmT3(getTnm3(conceptsPresent, involvedConcepts,
                tnmClassificationRequest.isSatelliteNodules()));
        tnmClassificationResponse.setTnmT4(getTnm4(conceptsPresent, involvedConcepts,
                tnmClassificationRequest.isTumorsInDiffentLobesIpsilateral()));

        tnmClassificationResponse.setTnmT(getTnm(tnmClassificationResponse));
        try {
            List<LymphNode> lymphNodes = lymphNodeClassificationService.getManualLymphNodes(lymphNodeConcepts);
            String n = lymphNodeClassificationService.classifyN(tnmClassificationRequest.getTumorSide(), lymphNodes);
            tnmClassificationResponse.setTnmN(n);
        } catch (Exception e) {
            logger.error("Unable to set N of TNM:", e);
        }

        return tnmClassificationResponse;
    }


    private boolean getTnm1(List<String> presentConcepts, List<String> involvementConcepts) {
        return !isIntersect(presentConcepts, tnmProperties.getConceptsPresentT1()) ||
                !isIntersect(involvementConcepts, tnmProperties.getConceptsInvolvementT1());
    }

    private boolean getTnm2(List<String> presentConcepts, List<String> involvementConcepts) {
        return !isIntersect(presentConcepts, tnmProperties.getConceptsPresentT2()) ||
                !isIntersect(involvementConcepts, tnmProperties.getConceptsInvolvementT2());
    }

    private boolean getTnm3(List<String> presentConcepts, List<String> involvementConcepts, boolean isSatelliteNodules ) {
        return !isIntersect(presentConcepts, tnmProperties.getConceptsPresentT3()) ||
                !isIntersect(involvementConcepts, tnmProperties.getConceptsInvolvementT3()) ||
                isSatelliteNodules;
    }

    private boolean getTnm4(List<String> presentConcepts, List<String> involvementConcepts, boolean isTumorsInDiffentLobesIpsilateral ) {
        return !isIntersect(presentConcepts, tnmProperties.getConceptsPresentT4()) ||
                !isIntersect(involvementConcepts, tnmProperties.getConceptsInvolvementT4()) ||
                isTumorsInDiffentLobesIpsilateral;
    }

    private boolean isIntersect(List<String> l1, List<String> l2){
        HashSet<String> s1 = new HashSet<>(l1);
        s1.retainAll(l2);
        return s1.isEmpty();
    }

}
