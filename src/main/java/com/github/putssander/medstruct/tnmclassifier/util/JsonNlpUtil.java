package com.github.putssander.medstruct.tnmclassifier.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.putssander.medstruct.tnmclassifier.classification.ConceptItem;
import com.github.putssander.medstruct.tnmclassifier.classification.MeasurementExpression;
import com.github.putssander.medstruct.tnmclassifier.constants.MedStructConstants;
import com.github.putssander.medstruct.tnmclassifier.constants.NlpConstants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static com.github.putssander.medstruct.tnmclassifier.constants.MedStructConstants.*;

public class JsonNlpUtil {


    public static JsonNode getJsonNode(String jsonNlp, ObjectMapper objectMapper) throws IOException {
        return objectMapper.readTree(jsonNlp);
    }


    public static JsonNode getDocument(JsonNode jsonNlpNode) {
        return jsonNlpNode.get("documents").get(0);
    }


    public static String refreshDocument(JsonNode jsonNlpNode, JsonNode document, ObjectMapper objectMapper) throws JsonProcessingException {
        ((ArrayNode) jsonNlpNode.withArray("documents")).set(0, document);
        return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNlpNode);
    }

    public static JsonNode addModuleResult(JsonNode document, JsonNode jsonNode, String key) {
        ((ObjectNode) document).put(key, jsonNode);
        return document;

    }

    public static JsonNode createJsonNode(Object object, ObjectMapper objectMapper){
        return objectMapper.valueToTree(object);
    }


    public static boolean isInSameSentence(JsonNode document, int token1, int token2) {
        return JsonNlpUtil.getSentenceId(document, token1) == (JsonNlpUtil.getSentenceId(document, token2));
    }

    public static Integer getSentenceId(JsonNode document, int token_id) {
        return document.get("tokenList").get(token_id - 1).get("sentence_id").asInt();
    }

    public static Integer getSentenceId(JsonNode token) {
        return token.get("sentence_id").asInt();
    }

    public static Integer getSentenceTokenFrom(JsonNode document, int sentenceId){
        return document.get("sentences").get(Integer.toString(sentenceId)).get("tokenFrom").asInt();
    }

    public static Integer getSentenceTokenTo(JsonNode document, int sentenceId){
        return document.get("sentences").get(Integer.toString(sentenceId)).get("tokenTo").asInt();
    }

    public static String getContextTargetUri(JsonNode contextElement) {
        return contextElement.get(LAYER_CONTEXT_TARGET).get(LAYER_CONTEXT_CATEGORY).get(0).textValue();
    }

    public static List<String> getTokenUris(JsonNode token) {
           List<String> uris = new ArrayList<>();
            if(token.get("misc").has("uri")) {
                Iterator<JsonNode> itr = token.get("misc").get("uri").iterator();
                while (itr.hasNext()) uris.add(itr.next().asText());
            }
            return uris;
    }

    public static String getTokenText(JsonNode token) {
        return token.get("text").asText();
    }

    public static List<Integer> getSentenceIdsForTokens(JsonNode document, List<Integer> tokens){
        return tokens.stream().map(token -> getSentenceId(document, token)).collect(Collectors.toList());
    }


    public static List<JsonNode> getTokensForSentenceId(JsonNode document, int sentenceId) {
        List<JsonNode> tokens = new ArrayList<>();
        document.get("tokenList").iterator().forEachRemaining(token -> {
            if(token.get("sentence_id").asInt() == sentenceId) tokens.add(token);
        });
        return tokens;
    }

    public static JsonNode getSentence(JsonNode document, int sentenceId) {
        return document.get("sentences").get(Integer.toString(sentenceId));
    }

    public static List<Integer> getUriSentenceIds(JsonNode document, String uri){
        List<Integer> sentenceIds = new ArrayList<>();
        document.get("tokenList").iterator().forEachRemaining(token -> {
            if(token.has("misc")
                    && token.get("misc").has("uri")) {
                Iterator<JsonNode> itr = token.get("misc").get("uri").iterator();
                while (itr.hasNext()) {
                    if(itr.next().textValue().equals(uri)){
                        sentenceIds.add(token.get("sentence_id").asInt());
                    }
                }
            }
        });
        return sentenceIds.stream().distinct().collect(Collectors.toList());
    }

    public static JsonNode getTokenById(JsonNode document, Integer id) {
        return document.get("tokenList").get(id - 1);
    }

    public static ConceptItem getTokenToConceptItem(JsonNode document, String uri, List<Integer> tokenIds) {
        ConceptItem conceptItem = new ConceptItem();
        conceptItem.setTokens(tokenIds);
        conceptItem.setUri(uri);
        conceptItem.setText(getTokensToString(document, tokenIds));
        return conceptItem;
    }

    public static String getTokensToString(JsonNode document, List<Integer> tokenIds){
        StringBuilder sb = new StringBuilder();
        for (int tokenId : tokenIds) {
            JsonNode token = document.get("tokenList").get(tokenId - 1);
            sb.append(token.get("text").asText());
            if(token.get("misc").get("SpaceAfter").asBoolean())
                sb.append(" ");
        }
        return sb.toString();
    }

    public static List<JsonNode> getTokensForUri(JsonNode document, String uri) {
        List<JsonNode> tokens = new ArrayList<>();
        document.get("tokenList").iterator().forEachRemaining(token -> {
            if(token.has("misc")
                    && token.get("misc").has("uri")) {
                Iterator<JsonNode> itr = token.get("misc").get("uri").iterator();
                while (itr.hasNext()) {
                    if(itr.next().textValue().equals(uri)){
                        tokens.add(token);
                    }
                }
            }
        });
        return tokens;
    }

    public static List<Integer> getTokensIdForUri(JsonNode document, String uri) {
        List<JsonNode> tokens = getTokensForUri(document, uri);
        return tokens.stream().map(token -> JsonNlpUtil.getTokenId(token)).collect(Collectors.toList());
    }

    public static List<ConceptItem> getConceptsForUri(JsonNode document, String uri) {
        List<ConceptItem> conceptItems = new ArrayList<>();
        document.get("tokenList").iterator().forEachRemaining(token -> {
            if(token.has("misc") && token.get("misc").has("uri")) {
                if(containsUri(token, uri)) {
                    if (token.get("entity_iob").asText().equals("B")) {
                        ConceptItem conceptItem = new ConceptItem();
                        conceptItem.setUri(uri);
                        conceptItem.getTokens().add(token.get("id").asInt());
                        conceptItems.add(conceptItem);
                    }
                    else {
                        if(!conceptItems.isEmpty())
                            conceptItems.get(conceptItems.size() -1).getTokens().add(token.get("id").asInt());
                    }
                }
            }
        });
        conceptItems.forEach(item ->
                item.setText(JsonNlpUtil.getTokensToString(document, item.getTokens())));
        return conceptItems;
    }

    private static boolean containsUri(JsonNode token, String uri){
        Iterator<JsonNode> itr = token.get("misc").get("uri").iterator();
        while (itr.hasNext()) {
            if(itr.next().textValue().equals(uri))
                return true;
        }
        return false;
    }

    public static boolean isTokensInSameSentence(JsonNode token1, JsonNode token2){
        return JsonNlpUtil.getSentenceId(token1) == JsonNlpUtil.getSentenceId(token2);
    }

    public static boolean isTokensInSameSentence(JsonNode document, int token1, int token2){
        return JsonNlpUtil.getSentenceId(getTokenById(document, token1))
                == JsonNlpUtil.getSentenceId(getTokenById(document, token2));
    }

    public static Integer getTokenId(JsonNode token) {
        return token.get("id").asInt();
    }

    public static String getSentenceString(JsonNode document, Integer sentenceId) {
        List<JsonNode> tokens = getTokensForSentenceId(document, sentenceId);
        StringBuilder stringBuilder = new StringBuilder();
        tokens.forEach(token -> {
            stringBuilder.append(token.get("text").asText());
            if(token.get("misc").get("SpaceAfter").asBoolean()){
                stringBuilder.append(" ");
            }
        });
        return stringBuilder.toString();
    }

    public static List<MeasurementExpression> getMeasurements(List<MeasurementExpression> measurements, String uri) {
        return measurements.stream()
                .filter(measurement -> measurement.getCode() != null && measurement.getCode().equals(uri))
                .collect(Collectors.toList());
    }

    public static List<MeasurementExpression> getMeasurements(ObjectMapper objectMapper, JsonNode document) throws IOException {
        String json = document.get(MedStructConstants.LAYER_MEASUREMENTS).toString();
        return objectMapper.readValue(json, new TypeReference<List<MeasurementExpression>>(){});
    }

    public static Integer getFirstPunctuationIdForward(JsonNode document, Integer tokenId) {
        Iterator<JsonNode> itr = document.get("tokenList").iterator();
        while (itr.hasNext()){
            JsonNode token = itr.next();
            int currentTokenId = token.get("id").asInt();

            if((currentTokenId >= tokenId) && token.get("upos").textValue().equals("PUNCT")){
                String lemma = token.get("lemma").asText();
                if(!lemma.equals("(") || !lemma.equals(")"))
                    return token.get("id").asInt();
            }
        }
        return tokenId;
    }

    public static Integer getFirstPunctuationIdBackwards(JsonNode document, Integer tokenId) {
        Integer sentenceTokenFrom = getSentenceTokenFrom(document, getSentenceId(document, tokenId));
        Integer punctuationTokenId = null;

        Iterator<JsonNode> itr = document.get("tokenList").iterator();
        while (itr.hasNext()){
           JsonNode current = itr.next();
           int itrTokenId = current.get("id").asInt();

           if(itrTokenId < sentenceTokenFrom)
               continue;

           if(itrTokenId >= tokenId)
                break;

           if(current.get("upos").textValue().equals("PUNCT"))
               punctuationTokenId = itrTokenId;
        }

        if(punctuationTokenId!=null)
            return punctuationTokenId;
        else
            return sentenceTokenFrom;
    }

    public static int getDocumentTokensEnd(JsonNode document) {
        Iterator<JsonNode> itr = document.get("tokenList").iterator();
        int size = 0;
        while (itr.hasNext()){
            itr.next();
            size++;
        }
        return size;
    }

    public static int getJoinTokenIdBackwards(JsonNode document, int lymphNodeTokenId) {
        int currentTokenId = lymphNodeTokenId - 1;
        while(currentTokenId > 1) {
            JsonNode token = getTokenById(document, currentTokenId);
            String upos = token.get("upos").asText();
            if(NlpConstants.posJoins.contains(upos)){
                return currentTokenId;
            }
            currentTokenId--;
        }
        return 1;
    }

    public static int getJoinTokenIdForwards(JsonNode document, int lymphNodeTokenId) {
        int currentTokenId = lymphNodeTokenId + 1;
        while(currentTokenId > 1) {
            JsonNode token = getTokenById(document, currentTokenId);
            String upos = token.get("upos").asText();
            if(NlpConstants.posJoins.contains(upos)){
                return currentTokenId;
            }
            currentTokenId++;
        }
        return getDocumentTokensEnd(document);
    }

    public static String getDocumentId(JsonNode document) {
        return document.get("meta").get("DC.identifier").asText();
    }
}

