package com.github.putssander.medstruct.tnmclassifier.classification;


import com.github.putssander.medstruct.tnmclassifier.constants.BodySide;

import java.util.ArrayList;
import java.util.List;

public class TnmClassification extends TnmClassificationResponse {

    private String version = TnmClassification.class.getPackage().getImplementationVersion();

    private String application = TnmClassification.class.getPackage().getName();

    private String tnm;

    private String tnmSize;

    private BodySide tumorSide;

    private double tumorSize;

    private List<ConceptItem> presentConcepts = new ArrayList<>();

    private List<ConceptItem> involvedConcepts = new ArrayList<>();

    private boolean satelliteNodules;

    private boolean tumorsInDiffentLobesIpsilateral;

    private List<LymphNode> lymphNodes = new ArrayList<>();

    private List<Tumor> tumors = new ArrayList<>();

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getTnm() {
        return tnm;
    }

    public void setTnm(String tnm) {
        this.tnm = tnm;
    }

    @Override
    public String getTnmSize() {
        return tnmSize;
    }

    @Override
    public void setTnmSize(String tnmSize) {
        this.tnmSize = tnmSize;
    }

    public BodySide getTumorSide() {
        return tumorSide;
    }

    public void setTumorSide(BodySide tumorSide) {
        this.tumorSide = tumorSide;
    }

    public double getTumorSize() {
        return tumorSize;
    }

    public void setTumorSize(double tumorSize) {
        this.tumorSize = tumorSize;
    }

    public List<ConceptItem> getPresentConcepts() {
        return presentConcepts;
    }

    public void setPresentConcepts(List<ConceptItem> presentConcepts) {
        this.presentConcepts = presentConcepts;
    }

    public List<ConceptItem> getInvolvedConcepts() {
        return involvedConcepts;
    }

    public void setInvolvedConcepts(List<ConceptItem> involvedConcepts) {
        this.involvedConcepts = involvedConcepts;
    }

    public boolean isSatelliteNodules() {
        return satelliteNodules;
    }

    public void setSatelliteNodules(boolean satelliteNodules) {
        this.satelliteNodules = satelliteNodules;
    }

    public boolean isTumorsInDiffentLobesIpsilateral() {
        return tumorsInDiffentLobesIpsilateral;
    }

    public void setTumorsInDiffentLobesIpsilateral(boolean tumorsInDiffentLobesIpsilateral) {
        this.tumorsInDiffentLobesIpsilateral = tumorsInDiffentLobesIpsilateral;
    }

    public List<LymphNode> getLymphNodes() {
        return lymphNodes;
    }

    public void setLymphNodes(List<LymphNode> lymphNodes) {
        this.lymphNodes = lymphNodes;
    }

    public List<Tumor> getTumors() {
        return tumors;
    }

    public void setTumors(List<Tumor> tumors) {
        this.tumors = tumors;
    }

    @Override
    public String toString() {
        return "TnmClassification{" +
                "version='" + version + '\'' +
                ", application='" + application + '\'' +
                ", tnm='" + tnm + '\'' +
                ", tnmSize='" + tnmSize + '\'' +
                ", tumorSide=" + tumorSide +
                ", tumorSize=" + tumorSize +
                ", presentConcepts=" + presentConcepts +
                ", involvedConcepts=" + involvedConcepts +
                ", satelliteNodules=" + satelliteNodules +
                ", tumorsInDiffentLobesIpsilateral=" + tumorsInDiffentLobesIpsilateral +
                ", lymphNodes=" + lymphNodes +
                ", tumors=" + tumors +
                '}';
    }
}
