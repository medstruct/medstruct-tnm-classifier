package com.github.putssander.medstruct.tnmclassifier.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "thoracic.lymph-node-station")
public class ThoracicLymphNodeStationProperties {

    private String one = "none:thorax-station-1";
    private String two = "none:thorax-station-2";
    private String threeA = "none:thorax-station-3a";
    private String threeP = "none:thorax-station-3p";
    private String four = "none:thorax-station-4";
    private String five = "none:thorax-station-5";
    private String six = "none:thorax-station-6";
    private String seven = "none:thorax-station-7";
    private String eight = "none:thorax-station-8";
    private String nine = "none:thorax-station-9";
    private String ten = "none:thorax-station-10";
    private String eleven = "none:thorax-station-11";
    private String twelve = "none:thorax-station-12";
    private String thirteen = "none:thorax-station-13";
    private String fourteen = "none:thorax-station-14";

    public String getOne() {
        return one;
    }

    public void setOne(String one) {
        this.one = one;
    }

    public String getTwo() {
        return two;
    }

    public void setTwo(String two) {
        this.two = two;
    }

    public String getThreeA() {
        return threeA;
    }

    public void setThreeA(String threeA) {
        this.threeA = threeA;
    }

    public String getThreeP() {
        return threeP;
    }

    public void setThreeP(String threeP) {
        this.threeP = threeP;
    }

    public String getFour() {
        return four;
    }

    public void setFour(String four) {
        this.four = four;
    }

    public String getFive() {
        return five;
    }

    public void setFive(String five) {
        this.five = five;
    }

    public String getSix() {
        return six;
    }

    public void setSix(String six) {
        this.six = six;
    }

    public String getSeven() {
        return seven;
    }

    public void setSeven(String seven) {
        this.seven = seven;
    }

    public String getEight() {
        return eight;
    }

    public void setEight(String eight) {
        this.eight = eight;
    }

    public String getNine() {
        return nine;
    }

    public void setNine(String nine) {
        this.nine = nine;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getEleven() {
        return eleven;
    }

    public void setEleven(String eleven) {
        this.eleven = eleven;
    }

    public String getTwelve() {
        return twelve;
    }

    public void setTwelve(String twelve) {
        this.twelve = twelve;
    }

    public String getThirteen() {
        return thirteen;
    }

    public void setThirteen(String thirteen) {
        this.thirteen = thirteen;
    }

    public String getFourteen() {
        return fourteen;
    }

    public void setFourteen(String fourteen) {
        this.fourteen = fourteen;
    }
}
