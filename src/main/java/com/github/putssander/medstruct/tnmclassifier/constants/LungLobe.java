package com.github.putssander.medstruct.tnmclassifier.constants;

public enum LungLobe {

    LUL, LLL, RUL, RML, RLL, UNKNOWN
}