package com.github.putssander.medstruct.tnmclassifier.classification;

import com.github.putssander.medstruct.tnmclassifier.constants.BodySide;


public class BodySideMention {

    private BodySide bodySide;

    private int firstTokenId;

    public BodySideMention(BodySide bodySide, int firstTokenId) {
        this.bodySide = bodySide;
        this.firstTokenId = firstTokenId;
    }

    public BodySide getBodySide() {
        return bodySide;
    }

    public void setBodySide(BodySide bodySide) {
        this.bodySide = bodySide;
    }

    public int getFirstTokenId() {
        return firstTokenId;
    }

    public void setFirstTokenId(int firstTokenId) {
        this.firstTokenId = firstTokenId;
    }
}
