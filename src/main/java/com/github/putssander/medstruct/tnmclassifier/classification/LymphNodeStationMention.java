package com.github.putssander.medstruct.tnmclassifier.classification;

import com.github.putssander.medstruct.tnmclassifier.constants.ThoracicLymphNodeStation;


public class LymphNodeStationMention {

    private ThoracicLymphNodeStation thoracicLymphNodeStation;

    private int tokenId;

    public LymphNodeStationMention(ThoracicLymphNodeStation thoracicLymphNodeStation, int tokenId) {
        this.thoracicLymphNodeStation = thoracicLymphNodeStation;
        this.tokenId = tokenId;
    }

    public ThoracicLymphNodeStation getThoracicLymphNodeStation() {
        return thoracicLymphNodeStation;
    }

    public void setThoracicLymphNodeStation(ThoracicLymphNodeStation thoracicLymphNodeStation) {
        this.thoracicLymphNodeStation = thoracicLymphNodeStation;
    }

    public int getTokenId() {
        return tokenId;
    }

    public void setTokenId(int tokenId) {
        this.tokenId = tokenId;
    }
}
